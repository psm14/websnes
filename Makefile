SNES9X_OBJECTS  = snes9x/apu/apu.o snes9x/apu/bapu/dsp/sdsp.o snes9x/apu/bapu/dsp/SPC_DSP.o snes9x/apu/bapu/smp/smp.o snes9x/apu/bapu/smp/smp_state.o snes9x/bsx.o \
									snes9x/c4.o snes9x/c4emu.o snes9x/clip.o snes9x/controls.o snes9x/cpu.o snes9x/cpuexec.o snes9x/cpuops.o snes9x/dma.o snes9x/dsp.o snes9x/dsp1.o snes9x/dsp2.o snes9x/dsp3.o \
									snes9x/dsp4.o snes9x/fxinst.o snes9x/fxemu.o snes9x/gfx.o snes9x/globals.o snes9x/memmap.o snes9x/obc1.o snes9x/ppu.o snes9x/sa1.o snes9x/sa1cpu.o snes9x/sha256.o \
									snes9x/sdd1.o snes9x/sdd1emu.o snes9x/seta.o snes9x/seta010.o snes9x/seta011.o snes9x/seta018.o snes9x/snes9x.o snes9x/spc7110.o snes9x/srtc.o snes9x/tile.o \
									snes9x/snapshot.o snes9x/movie.o snes9x/msu1.o snes9x/cheats.o snes9x/cheats2.o snes9x/logger.o snes9x/stream.o snes9x/bml.o snes9x/screenshot.o snes9x/crosshairs.o \
									snes9x/filter/blit.o snes9x/filter/2xsai.o snes9x/filter/epx.o snes9x/filter/hq2x.o snes9x/filter/snes_ntsc.o

EMSNES_OBJECTS = emsnes/em_audio.o emsnes/em_input.o emsnes/em_video.o emsnes/em_io.o emsnes/em_main.o

OBJECTS = $(SNES9X_OBJECTS) $(EMSNES_OBJECTS)

INCLUDES = -Isnes9x -Iemsnes -Isnes9x/apu/ -Isnes9x/filter/
CCFLAGS = -U__linux -O3 -DLSB_FIRST  -fomit-frame-pointer -fno-exceptions -fno-rtti \
					-pedantic -Wall -W -Wno-unused-parameter -D_GNU_SOURCE=1 -D_REENTRANT \
					-DHAVE_MKSTEMP -DHAVE_STRINGS_H -DHAVE_SYS_IOCTL_H -DHAVE_STDINT_H \
					-DRIGHTSHIFT_IS_SAR -Wno-c++11-extensions --llvm-lto 1

EMCCFLAGS = -O3 -lm

LINKFLAGS = --pre-js emsnes/js/build/app.bundle.js \
						-s TOTAL_MEMORY=50331648 \
						-s MODULARIZE=1 \
						-s SINGLE_FILE=1 \
						-s USE_SDL=0 \
						-s EXPORT_NAME=SNES9X \
						--llvm-lto 1

EMCC = emcc

.SUFFIXES: .o .cpp .c .cc .h .m .i .s .obj

all: websnes

dev: websnes/src/gen/engine.dat websnes/node_modules

websnes: websnes/src/gen/engine.dat websnes/node_modules
	cd websnes; npm run build

websnes/node_modules:
	cd websnes; npm install

emsnes/js/build/app.bundle.js: emsnes/js/node_modules
	cd emsnes/js; npm run build

emsnes/js/node_modules:
	cd emsnes/js; npm install

websnes/src/gen/engine.dat: websnes/src/gen/snes9x.js
	cp websnes/src/gen/snes9x.js $@

websnes/src/gen:
	mkdir -p $@

# Also produces snes9x.wasm
websnes/src/gen/snes9x.js: websnes/src/gen emsnes/js/build/app.bundle.js $(OBJECTS)
	$(EMCC) $(EMCCFLAGS) $(LINKFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(EMCC) $(EMCCFLAGS) $(INCLUDES) $(CCFLAGS) $*.cpp -o $@

.c.o:
	$(EMCC) $(EMCCFLAGS) $(INCLUDES) $(CCFLAGS) $*.c -o $@

clean:
	rm -rf websnes/build websnes/src/gen emsnes/js/build emsnes/js/node_modules $(OBJECTS)

.phony: all clean websnes dev
