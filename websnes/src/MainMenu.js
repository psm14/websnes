import React from 'react';
import AppBar from './AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import { withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import 'typeface-roboto';
import NavigationBar from './NavigationBar';
import RomList from './RomList';
import Settings from './settings/Settings';
import { compose, withHandlers, withState, defaultProps } from 'recompose';
import DEFAULT_THEME from './lib/theme';

const styles = (theme) => ({
  toolbar: {
    ...theme.mixins.toolbar,
    '@supports (height: env(safe-area-inset-top))': {
      height: `calc(${theme.spacing.unit * 8}px + env(safe-area-inset-top))`,
      paddingTop: 'env(safe-area-inset-top)',
    },
  },
  footer: {
    '@supports not (height: env(safe-area-inset-bottom))': {
      height: theme.spacing.unit * 7,
    },
    '@supports (height: env(safe-area-inset-bottom))': {
      height: `calc(${theme.spacing.unit * 7}px + env(safe-area-inset-bottom))`,
    },
  },
  content: {
    padding: theme.spacing.unit * 3,
    '@supports (padding-left: env(safe-area-inset-bottom))': {
      paddingLeft: `max(${theme.spacing.unit * 3}px, env(safe-area-inset-left))`,
      paddingRight: `max(${theme.spacing.unit * 3}px, env(safe-area-inset-right))`,
    },
  },
});

const enhance = compose(
  defaultProps({ theme: DEFAULT_THEME }),
  withStyles(styles),
  withState('navItem', 'setNavItem', 0),
  withHandlers({
    onPlay: ({ setRom, setShowSnes }) => (name) => {
      setRom(name);
      setShowSnes(true);
    },
    onChange: ({ setNavItem }) => (e, value) => setNavItem(value),
  }),
);

const ContentContainer = ({ classes, children }) => (
  <main className={ classes.content }>
    <div className={ classes.toolbar }/>
    { children }
    <div className={ classes.footer }/>
  </main>
);

const NavItem = ({ navItem, onPlay, snesLoaded, settings,
                   setSettings }) => {
  if (navItem === 0) {
    return (
      <RomList onPlay={ onPlay } snesLoaded={ snesLoaded }/>
    );
  } else {
    return (
      <Settings settings={ settings } setSettings={ setSettings }/>
    );
  }
}

const MainMenu = ({ onPlay, onChange, navItem, setShowSnes, classes, snesLoaded,
                    drawerOpen, theme, settings, setSettings }) => (
  <MuiThemeProvider theme={ theme }>
    <div>
      <CssBaseline />
      <AppBar setShowSnes={ setShowSnes } snesLoaded={ snesLoaded }/>
      <ContentContainer classes={ classes } drawerOpen={ drawerOpen }>
        <NavItem navItem={ navItem } onPlay={ onPlay }
                 settings={ settings } setSettings={ setSettings }
                 snesLoaded={ snesLoaded }/>
      </ContentContainer>
      <NavigationBar onChange={ onChange } navItem={ navItem }/>
    </div>
  </MuiThemeProvider>
)

export default enhance(MainMenu);
