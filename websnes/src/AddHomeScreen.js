//import React, { Component } from 'react';
import { Component } from 'react';

class AddHomeScreen extends Component {
  state = {
    listener: null,
    prompt: null,
  }

  componentDidMount() {
    let listener = (e) => {
      console.log("Get before install prompt event");
      e.preventDefault();
      this.setState({ prompt: e });
    }
    window.addEventListener('beforeinstallprompt', listener);
    this.setState({ listener });
  }

  componentWillUnmount() {
    let { listener } = this.state;
    if (listener) {
      window.removeEventListener('beforeinstallprompt', listener);
    }
  }

  handleClick(e) {
    let { prompt } = this.state;
    if (prompt) {
      prompt.prompt();
      prompt.userChoice
            .then((choiceResult) => {
              if (choiceResult.outcome === 'accepted') {
                console.log('User accepted the A2HS prompt');
              } else {
                console.log('User dismissed the A2HS prompt');
              }
              this.setState({ prompt: null });
            });
    }
  }

  render() {
    //let { prompt } = this.state;
    //let enabled = Boolean(prompt);
    //let handleClick = this.handleClick.bind(this);
    return null;
    /*return (
      <button className="add-to-home-screen" disabled={ !enabled }
              onClick={ handleClick }>
        Add to Home
      </button>
    )*/
  }
}

export default AddHomeScreen
