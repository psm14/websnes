import React, { Component } from 'react';
import TouchPad from './TouchPad';
import { withStyles } from '@material-ui/core/styles';
import { compose } from 'recompose';

const styles = {
  catcher: {
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
    width: '100%',
    height: '100%',
    '@supports (padding-left: env(safe-area-inset-left))': {
      paddingTop: 'env(safe-area-inset-top)',
      paddingLeft: 'env(safe-area-inset-left)',
      paddingRight: 'env(safe-area-inset-right)',
      paddingBottom: 'env(safe-area-inset-bottom)',
    },
  },
  padContainer: {
    position: 'relative',
    width: '100%',
    height: '100%',
  },
}

const enhance = compose(
  withStyles(styles),
);

const swallowTouch = (e) => e.preventDefault();

class TouchButtons extends Component {
  constructor(props) {
    super(props);
    this.state = {
      catcher: React.createRef(),
    }
  }

  componentDidMount() {
    let { catcher: { current: catcher } } = this.state;
    catcher.addEventListener('touchstart', swallowTouch);
    catcher.addEventListener('touchmove', swallowTouch);
    catcher.addEventListener('touchend', swallowTouch);
    catcher.addEventListener('touchcancel', swallowTouch);
  }

  componentWillUnmount() {
    let { catcher: { current: catcher } } = this.state;
    catcher.removeEventListener('touchstart', swallowTouch);
    catcher.removeEventListener('touchmove', swallowTouch);
    catcher.removeEventListener('touchend', swallowTouch);
    catcher.removeEventListener('touchcancel', swallowTouch);
  }

  render() {
    let { catcher } = this.state;
    let { bouncer, classes, settings } = this.props;

    return (
      <div className={ classes.catcher } ref={ catcher }>
        <div className={ classes.padContainer }>
          <TouchPad bouncer={ bouncer } isLeft={ true } settings={ settings }/>
          <TouchPad bouncer={ bouncer } isLeft={ false } settings={ settings }/>
        </div>
      </div>
    )
  }
}

const EnhancedTouchButtons = enhance(TouchButtons);

const TouchWrapper = (props) => {
  if (props.settings.enabled) {
    return (<EnhancedTouchButtons { ...props }/>);
  } else {
    return null;
  }
}

export default TouchWrapper;
