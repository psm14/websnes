import React from 'react';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import Paper from '@material-ui/core/Paper';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import FolderIcon from '@material-ui/icons/Folder';
import SettingsIcon from '@material-ui/icons/Settings';
import { withStyles } from '@material-ui/core/styles';
import { compose } from 'recompose';

const styles = (theme) => ({
  navigation: {
    ...theme.mixins.gutters(),
    position: 'fixed',
    bottom: 0,
    left: 0,
    right: 0,
    '@supports not (height: env(safe-area-inset-bottom))': {
      height: theme.spacing.unit * 7,
    },
    '@supports (height: env(safe-area-inset-bottom))': {
      height: `calc(${theme.spacing.unit * 7}px + env(safe-area-inset-bottom))`,
      paddingBottom: 'env(safe-area-inset-bottom)',
    },
  }
});

const enhance = compose(
  withStyles(styles),
);

const NavigationBar = ({ onChange, navItem, classes }) => (
  <Paper className={ classes.navigation } elevation={ 2 }>
    <BottomNavigation
      value={navItem}
      onChange={onChange}
      showLabels>
      <BottomNavigationAction label="ROMs" icon={<FolderIcon />} />
      <BottomNavigationAction label="Settings" icon={<SettingsIcon />} />
    </BottomNavigation>
  </Paper>
);

export default enhance(NavigationBar);
