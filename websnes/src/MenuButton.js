import React from 'react';
import { withHandlers, compose } from 'recompose';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  menuButton: {
    borderColor: '#666666',
    borderWidth: 4,
    borderTopStyle: 'solid',
    borderLeftStyle: 'solid',
    position: 'absolute',
    right: 0,
    bottom: 0,
    width: 50,
    height: 40,
    opacity: 0.5,
    borderTopLeftRadius: 10,
    color: '#666666',
    fontSize: '30px',
    textAlign: 'center',
    verticalAlign: 'middle',
    fontWeight: 800,
    fontFamily: 'Arial, Helvetica, sans-serif',
  },
}

const enhance = compose(
  withStyles(styles),
  withHandlers({
    onMenuClick: ({ setShowSnes }) => () => setShowSnes(false),
  }),
);

const MenuButton = ({ onMenuClick, classes }) => (
  <div className={ classes.menuButton } onClick={ onMenuClick }>☰</div>
);

export default enhance(MenuButton);
