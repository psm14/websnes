import React from 'react';
import SnesLoader from './snes/SnesLoader';
import Screen from './Screen';
import TouchButtons from './TouchButtons';
import MenuButton from './MenuButton';
import ControllerInput from './lib/input/ControllerInput';
import KeyInput from './lib/input/KeyInput';
import InputManager from './lib/input/InputManager';
import { withState, withPropsOnChange, compose, withHandlers } from 'recompose';
import { withStyles } from '@material-ui/core/styles';
import MainMenu from './MainMenu';
import * as settings from './lib/settings';
import shallowEq from 'shallow-equal/objects';

const styles = {
  panel: {
    backgroundColor: '#0a0a0a',
    position: 'fixed',
    zIndex: 5000,
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
    padding: 0,
    overflow: 'hidden',
    transition: 'all 0.4s ease-in-out',
    '@supports (padding-top: env(safe-area-inset-top))': {
      paddingTop: 'env(safe-area-inset-top)',
      paddingLeft: 'env(safe-area-inset-left)',
      paddingRight: 'env(safe-area-inset-right)',
      paddingBottom: 'env(safe-area-inset-bottom)',
    },
  },
  panelHide: {
    transform: 'translateY(-100vh)',
    opacity: 0,
  }
}

const getSettings = (name, def) => {
  if (window.localStorage) {
    return JSON.parse(window.localStorage.getItem(name)) || def;
  } else {
    return def;
  }
}

const putSettings = (name, value) => {
  if (window.localStorage) {
    window.localStorage.setItem(name, JSON.stringify(value));
  }
}

const withSettings = compose(
  withState('touchSettings', '_setTouchSettings', getSettings('touchSettings', settings.DEFAULT_TOUCH_SETTINGS)),
  withState('graphicsSettings', '_setGraphicsSettings', getSettings('graphicsSettings', settings.DEFAULT_GRAPHICS_SETTINGS)),
  withState('keyboardSettings', '_setKeyboardSettings', getSettings('keyboardSettings', settings.DEFAULT_KEYBOARD_SETTINGS)),
  withState('gamepadSettings', '_setGamepadSettings', getSettings('gamepadSettings', settings.DEFAULT_GAMEPAD_SETTINGS)),
  withPropsOnChange(['touchSettings', 'graphicsSettings', 'keyboardSettings', 'gamepadSettings'], ({ touchSettings, graphicsSettings, keyboardSettings, gamepadSettings }) => ({
    settings: {
      touch: touchSettings,
      graphics: graphicsSettings,
      keyboard: keyboardSettings,
      gamepad: gamepadSettings,
    },
  })),
  withHandlers({
    setSettings: ({ touchSettings, graphicsSettings, keyboardSettings, gamepadSettings,
                    _setTouchSettings, _setGraphicsSettings, _setKeyboardSettings, _setGamepadSettings }) => (settings) => {
      let { touch, graphics, keyboard, gamepad } = settings;
      if (!shallowEq(touch, touchSettings)) {
        putSettings('touchSettings', touch);
        _setTouchSettings(touch);
      }
      if (!shallowEq(graphics, graphicsSettings)) {
        putSettings('graphicsSettings', graphics);
        _setGraphicsSettings(graphics);
      }
      if (!shallowEq(keyboard, keyboardSettings)) {
        putSettings('keyboardSettings', keyboard);
        _setKeyboardSettings(keyboard);
      }
      if (!shallowEq(gamepad, gamepadSettings)) {
        putSettings('gamepadSettings', gamepad);
        _setGamepadSettings(gamepad);
      }
    },
  }),
)

const enhance = compose(
  withStyles(styles),
  withPropsOnChange([], () => ({ canvas: React.createRef(),
                                 bouncer: new InputManager() })),
  withState('rom', 'setRom', null),
  withState('showSnes', 'setShowSnes', false),
  withState('snesLoaded', 'setSnesLoaded', false),
  withSettings,
);

const SnesPanel = ({ children, show, classes }) => {
  let classNames = [classes.panel, show ? null : classes.panelHide].join(' ');
  return (
    <div className={ classNames }>
      { children }
    </div>
  );
} 

const App = ({ canvas, rom, setRom, bouncer, showSnes, setShowSnes, classes,
               settings, setSettings, snesLoaded, setSnesLoaded }) => (
  <>
  <MainMenu setShowSnes={ setShowSnes } setRom={ setRom }
            settings={ settings } setSettings={ setSettings }
            snesLoaded={ snesLoaded }/>
  <SnesPanel show={ showSnes } classes={ classes }>
    <Screen canvasRef={ canvas }/>
    <TouchButtons bouncer={ bouncer } settings={ settings.touch }/>
    <MenuButton setShowSnes={ setShowSnes }/>
  </SnesPanel>
  <KeyInput bouncer={ bouncer } settings={ settings.keyboard } enabled={ showSnes && settings.keyboard.enabled }/>
  <ControllerInput bouncer={ bouncer } settings={ settings.gamepad } enabled={ showSnes && settings.gamepad.enabled }/>
  <SnesLoader rom={ rom } canvas={ canvas } bouncer={ bouncer } paused={ !showSnes }
              setLoadState={ setSnesLoaded } graphicsSettings={ settings.graphics }/>
  </>
);

export default enhance(App);
