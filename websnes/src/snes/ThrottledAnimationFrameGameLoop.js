import { Component } from 'react';

const FRAME_INTERVAL = 1000 / 60; /* ms per frame */

class SnesGameLoop extends Component {
  constructor(props) {
    super(props);

    this.visibilityListener = this.visibilityListener.bind(this);
    this.gameloop = this.gameloop.bind(this);

    this.then = Date.now();

    this.visible = !document.hidden;
    // Arrrgh
    this.mounted = false;
  }

  gameloop() {
    if (!this.mounted) {
      return;
    } else if (this.props.runLoop && this.visible) {
      let now = Date.now();
      let delta = now - this.then;
      if (delta >= FRAME_INTERVAL) {
        let { bouncer, snes } = this.props;
        bouncer.poll();
        snes._mainloop();
        this.then = now - (delta % FRAME_INTERVAL);
      }
    }
    requestAnimationFrame(this.gameloop);
  }

  visibilityListener() {
    this.visible = !document.hidden;
    let { snes, runLoop } = this.props;
    if (runLoop) {
      snes.audio.setMute(!this.visible);
    }
  }

  componentDidMount() {
    this.mounted = true;
    document.addEventListener('visibilitychange', this.visibilityListener);
    requestAnimationFrame(this.gameloop);
  }

  componentWillUnmount() {
    this.mounted = false;
    document.removeEventListener('visibilitychange', this.visibilityListener);
  }

  render() {
    return null;
  }
}

export default SnesGameLoop;
