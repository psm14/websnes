import { withEventListener, DummyElement } from '../lib/recompose-extras';
import { compose, withHandlers } from 'recompose';

const withAudioUnlocker = compose(
  withHandlers({
    unlockAudio: ({ snes }) => () => {
      let context = snes.audio.context;
      if (context.state === 'suspended') {
        context.resume();
      }
    }
  }),
  withEventListener(document.body, ['touchstart', 'touchend'], 'unlockAudio'),
);

export default withAudioUnlocker(DummyElement);
