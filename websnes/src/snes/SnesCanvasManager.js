import { Component } from 'react';

class SnesCanvasManager extends Component {
  componentDidMount() {
    let { snes, canvas } = this.props;
    snes.display.updateCanvas(canvas.current);
  }

  componentDidUpdate(prevProps) {
    let { prevSnes } = prevProps;
    if (prevSnes) {
      prevSnes.display.updateCanvas(null);
    }

    let { snes, canvas, graphicsSettings } = this.props;
    let currentMode = snes.display.videoMode();
    if (graphicsSettings && graphicsSettings.filter !== currentMode) {
      snes.display.setVideoMode(graphicsSettings.filter);
    }

    snes.display.updateCanvas(canvas.current);
  }

  componentWillUnmount() {
    let { snes } = this.props;
    snes.display.updateCanvas(null);
  }

  render() {
    return null;
  }
}

export default SnesCanvasManager;
