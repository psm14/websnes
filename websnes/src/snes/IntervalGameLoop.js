import { Component } from 'react';

const FRAME_INTERVAL = 1000 / 60; /* ms per frame */

class SnesGameLoop extends Component {
  constructor(props) {
    super(props);

    this.visibilityListener = this.visibilityListener.bind(this);
    this.gameloop = this.gameloop.bind(this);

    this.visible = !document.hidden;
    this.interval = null;
  }

  stopLoop() {
    if (this.interval) {
      clearInterval(this.interval);
      this.interval = null;
    }
  }

  startLoop() {
    if (!this.interval) {
      this.interval = setInterval(this.gameloop, FRAME_INTERVAL);
    }
  }

  gameloop() {
    let { bouncer, snes } = this.props;
    bouncer.poll();
    snes._mainloop();
  }

  visibilityListener() {
    this.visible = !document.hidden;
    let { snes, runLoop } = this.props;
    if (runLoop) {
      snes.audio.setMute(!this.visible);
    }
    this.componentDidUpdate();
  }

  componentDidMount() {
    this.mounted = true;
    document.addEventListener('visibilitychange', this.visibilityListener);
    this.componentDidUpdate();
  }

  componentDidUpdate() {
    let { runLoop } = this.props;
    if (runLoop && this.visible) {
      this.startLoop();
    } else {
      this.stopLoop();
    }
  }

  componentWillUnmount() {
    this.mounted = false;
    document.removeEventListener('visibilitychange', this.visibilityListener);
    this.stopLoop();
  }

  render() {
    return null;
  }
}

export default SnesGameLoop;
