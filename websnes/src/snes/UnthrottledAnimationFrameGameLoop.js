import { Component } from 'react';

class SnesGameLoop extends Component {
  constructor(props) {
    super(props);

    this.visibilityListener = this.visibilityListener.bind(this);
    this.gameloop = this.gameloop.bind(this);

    this.visible = !document.hidden;
    // Arrrgh
    this.mounted = false;
  }

  gameloop() {
    let { bouncer, snes, runLoop } = this.props;
    if (!this.mounted) {
      return;
    } else if (runLoop && this.visible) {
      bouncer.poll();
      snes._mainloop();
    }
    requestAnimationFrame(this.gameloop);
  }

  visibilityListener() {
    this.visible = !document.hidden;
    let { snes, runLoop } = this.props;
    if (runLoop) {
      snes.audio.setMute(!this.visible);
    }
  }

  componentDidMount() {
    this.mounted = true;
    document.addEventListener('visibilitychange', this.visibilityListener);
    requestAnimationFrame(this.gameloop);
  }

  componentWillUnmount() {
    this.mounted = false;
    document.removeEventListener('visibilitychange', this.visibilityListener);
  }

  render() {
    return null;
  }
}

export default SnesGameLoop;
