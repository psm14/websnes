import React, { Component } from 'react';
import IntervalGameLoop from './IntervalGameLoop';
import ThrottledAnimationFrameGameLoop from './ThrottledAnimationFrameGameLoop';
import UnthrottledAnimationFrameGameLoop from './UnthrottledAnimationFrameGameLoop';

const LOOP_INTERVAL = 0;
const LOOP_UNTHROTTLED = 1;
const LOOP_THROTTLED = 2;

const DETECTOR_SECONDS = 5;

const FPS_HIGH_CUTOFF = 65; // Usually if you're above 60hz, youre a ways above it
const FPS_LOW_CUTOFF  = 50;

const detectFrameRate = () => new Promise((resolve, reject) => {
  let frames = 0;
  let then = Date.now();
  let stop = then + (DETECTOR_SECONDS * 1000);
  let count = () => {
    frames += 1;
    let now = Date.now();
    if (now >= stop) {
      let seconds = (now - then) / 1000;
      let fps = frames / seconds;
      resolve(fps);
    } else {
      requestAnimationFrame(count);
    }
  }
  requestAnimationFrame(count);
});

class AutoSnesGameLoop extends Component {
  state = {
    loop: LOOP_INTERVAL,
  }

  async componentDidMount() {
    let fps = await detectFrameRate();
    console.log('Detected frame rate: ' + fps);
    let loop = LOOP_UNTHROTTLED;
    if (fps >= FPS_HIGH_CUTOFF) {
      console.log('Frame rate too high, using throttled game loop');
      loop = LOOP_THROTTLED;
    } else if (fps < FPS_LOW_CUTOFF) {
      console.log('Frame rate too low, using setInterval');
      loop = LOOP_INTERVAL;
    }
    this.setState({ loop });
  }

  render() {
    let { loop } = this.state;
    switch (loop) {
      case LOOP_INTERVAL:
        return (<IntervalGameLoop { ...this.props }/>);
      case LOOP_THROTTLED:
        return (<ThrottledAnimationFrameGameLoop { ...this.props }/>);
      default:
        return (<UnthrottledAnimationFrameGameLoop { ...this.props }/>);
    }
  }
}

export default AutoSnesGameLoop