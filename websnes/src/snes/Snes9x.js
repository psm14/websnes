import { withStorageConnection } from '../lib/storage';
import { withPropChangeHandler, DummyElement } from '../lib/recompose-extras';
import { compose, defaultProps, withState } from 'recompose';

const enhanceSnes = compose(
  defaultProps({ rom: null }),
  withStorageConnection(),
  withState('pointer', 'setPointer', 0),
  withState('loadPromise', 'setLoadPromise', null),
  withPropChangeHandler(({ rom: prevRom }, { snes, storage, pointer, setPointer, setRunLoop,
                                             rom, loadPromise, setLoadPromise }) => {
    if (rom === prevRom || !storage) {
      return;
    } else if (!rom) {
      // TODO: Actually stop
      setRunLoop(false);
      return;
    }

    let load = async () => {
      if (pointer) {
        setRunLoop(false);
        snes.fileio.freeBuffer(pointer);
      }

      // Is this needed?
      snes.audio.syncAudio();
      snes.audio.setMute(false);

      let romInfo = await storage.getRom(rom);
      if (!romInfo) { return; }

      let saveData = async (buffer) => {
        if (storage) {
          await storage.saveSRAM(rom, buffer);
          romInfo.sram = buffer;
        } else {
          console.warn('Couldn\'t save SRAM!');
        }
      };

      let loadData = () => {
        return romInfo.sram;
      };

      snes.fileio.saveData = saveData;
      snes.fileio.loadData = loadData;

      pointer = snes.fileio.injectBuffer(romInfo.rom);
      setPointer(pointer);
      snes._run(pointer);

      setRunLoop(true);

      // This still doesn't seem right. Whatever
      setLoadPromise(null);
    }

    if (loadPromise) {
      setLoadPromise(loadPromise.then(load));
    } else {
      setLoadPromise(load());
    }
  }),
)

export default enhanceSnes(DummyElement);
