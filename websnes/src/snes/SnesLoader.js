import React, { Component } from 'react';
import Snes9x from './Snes9x';
import SnesAudioUnlocker from './SnesAudioUnlocker';
import AutoSnesGameLoop from './AutoSnesGameLoop';
import SnesInputManager from './SnesInputManager';
import SnesCanvasManager from './SnesCanvasManager';
import { withState } from 'recompose';

// Nasty WebPack workaround
import snes9xengine from '../gen/engine.dat';

const enhance = withState('runLoop', 'setRunLoop', false);

class SnesLoader extends Component {
  state = {
    snes: null,
  }

  loadNotify(loaded) {
    let { setLoadState } = this.props;
    if (setLoadState) {
      setLoadState(loaded);
    }
  }

  snesLoaded() {
    let snes = new window.SNES9X();
    snes.onSnesLoad = () => {
      window.thesnes = snes;
      this.setState({ snes });
      this.loadNotify(true);
    }
  }

  componentDidMount() {
    if (!window.SNES9X) {
      let script = document.createElement('script');
      script.setAttribute('type', 'text/javascript');
      script.id = "snes9x-script";
      script.src = snes9xengine;
      script.addEventListener('load', () => {
        this.snesLoaded();
      });
      document.getElementsByTagName("head")[0].appendChild(script);
      this.loadNotify(false);
    } else {
      this.snesLoaded();
    }
  }

  render() {
    let { runLoop, bouncer, canvas, rom, setRunLoop, graphicsSettings,
          paused } = this.props;
    let { snes } = this.state;
    if (snes) {
      return (
        <>
          <Snes9x snes={ snes } rom={ rom } setRunLoop={ setRunLoop }/>
          <AutoSnesGameLoop snes={ snes } runLoop={ !paused && runLoop }
                            bouncer={ bouncer }/>
          <SnesInputManager snes={ snes } bouncer={ bouncer }/>
          <SnesCanvasManager snes={ snes } canvas={ canvas }
                             graphicsSettings={ graphicsSettings }/>
          <SnesAudioUnlocker snes={ snes }/>
        </>
      );
    } else {
      return null;
    }
  }
}

export default enhance(SnesLoader);
