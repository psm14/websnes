import { Component } from 'react';

class SnesInputManager extends Component {
  componentDidMount() {
    let { bouncer, snes } = this.props;
    let sendButton = snes._report_button.bind(snes);
    bouncer.setSendButton(sendButton);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.bouncer) {
      prevProps.bouncer.setSendButton(null);
    }

    let { bouncer, snes } = this.props;
    let sendButton = snes._report_button.bind(snes);
    bouncer.setSendButton(sendButton);
  }

  componentWillUnmount() {
    this.props.bouncer.setSendButton(null);
  }

  render() {
    return null;
  }
}

export default SnesInputManager;
