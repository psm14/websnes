import React from 'react';
import { compose } from 'recompose';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  container: {
    position: 'relative',
    margin: '0 auto',
    '@media not screen and (min-aspect-ratio: 8/7)': {
      height: 0,
      width: '100%',
      paddingTop: '87.5%',
    },
    '@media screen and (min-aspect-ratio: 8/7)': {
      width: 'calc(100vh * 1.14285)',
      height: '100vh',
    },
  },
  screen: {
    position: 'absolute',
    backgroundColor: '#000000',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    width: '100%',
    height: '100%',
  }
}

const enhanceScreen = compose(
  withStyles(styles),
);

const Screen = ({ canvasRef, classes }) => (
  <div className={ classes.container }>
    <canvas ref={ canvasRef } className={ classes.screen }/>
  </div>
);

export default enhanceScreen(Screen);
