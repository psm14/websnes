import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';

import { withHandlers, compose } from 'recompose';

const styles = (theme) => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    [theme.breakpoints.down('xs')]: {
      '@supports not (padding-left: env(safe-area-inset-left))': {
        paddingLeft: theme.spacing.unit * 3,
        paddingRight: theme.spacing.unit * 3,
        paddingTop: 0,
        height: theme.spacing.unit * 7,
      },
      '@supports (padding-left: env(safe-area-inset-left))': {
        paddingLeft: `calc(max(0px, env(safe-area-inset-left) - ${theme.spacing.unit * 3}px))`,
        paddingRight: `calc(max(0px, env(safe-area-inset-right) - ${theme.spacing.unit * 3}px))`,
        paddingTop: `env(safe-area-inset-top)`,
        height: `calc(${theme.spacing.unit * 7}px + env(safe-area-inset-top))`,
      },
    },
    [theme.breakpoints.up('sm')]: {
      '@supports not (padding-left: env(safe-area-inset-left))': {
        paddingLeft: theme.spacing.unit * 3,
        paddingRight: theme.spacing.unit * 3,
        paddingTop: 0,
        height: theme.spacing.unit * 8,
      },
      '@supports (padding-left: env(safe-area-inset-left))': {
        paddingLeft: `calc(max(0px, env(safe-area-inset-left) - ${theme.spacing.unit * 3}px))`,
        paddingRight: `calc(max(0px, env(safe-area-inset-right) - ${theme.spacing.unit * 3}px))`,
        paddingTop: `env(safe-area-inset-top)`,
        height: `calc(${theme.spacing.unit * 8}px + env(safe-area-inset-top))`,
      },
    },
  },
  progress: {
    marginRight: theme.spacing.unit * 2,
  },
  grow: {
    flexGrow: 1,
  },
});

const enhance = compose(
  withHandlers({
    onClickShow: ({ setShowSnes }) => () => setShowSnes(true),
  }),
  withStyles(styles),
);

const ShowButton = ({ classes, snesLoaded, onClick }) => {
  if (snesLoaded) {
    return (<Button color="inherit" onClick={ onClick }>Show</Button>);
  } else {
    return (<CircularProgress size={ 24 } className={ classes.progress }
                              color="inherit" />);
  }
}

const ButtonAppBar = ({ classes, onClickShow, snesLoaded }) => {
  return (
    <AppBar position="fixed" className={ classes.appBar }>
      <Toolbar>
        <Typography variant="h6" color="inherit" className={classes.grow}>
          Snes9x
        </Typography>
        <ShowButton classes={ classes } snesLoaded={ snesLoaded } onClick={ onClickShow }/>
      </Toolbar>
    </AppBar>
  );
}

export default enhance(ButtonAppBar);
