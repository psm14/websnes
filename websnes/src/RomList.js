import React from 'react';
import Dropzone from 'react-dropzone';
import { withStorageConnection } from './lib/storage';
import { withState, withHandlers, compose } from 'recompose';
import RomItem from './RomItem';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const styles = (theme) => ({
  placeholder: {
    marginTop: theme.spacing.unit * 6,
    marginBottom: theme.spacing.unit * 3,
    fontSize: '1.3em',
  },
  addButton: {
    marginTop: theme.spacing.unit * 3,
  },
});

const updateList = async ({ setRoms, storage }) => {
  if (storage) {
    let roms = await storage.listRoms();
    setRoms(roms);
  }
};

const enhanceList = compose(
  withState('roms', 'setRoms', []),
  withStorageConnection(updateList),
  withHandlers({
    onDrop: (props) => (accepted) => {
      let { storage } = props;
      accepted.forEach((file) => {
        if (storage) {
          let reader = new FileReader();
          reader.onload = async () => {
            let src = new Uint8Array(reader.result);
            await storage.addRom(file.name, src);
            await updateList(props);
          }
          reader.readAsArrayBuffer(file);
        }
      });
    },
    doUpdate: (props) => () => updateList(props),
  }),
  withStyles(styles),
);

const RomList = ({ roms, onPlay, doUpdate, storage, classes, snesLoaded }) => {
  if (roms.length === 0) {
    return (
      <Typography variant="body1" component="p" className={ classes.placeholder }>
        Add ROMs to get started.
      </Typography>
    )
  }
  return roms.map((rom) => <RomItem rom={ rom } onPlay={ onPlay }
                                    key={ rom.name }
                                    storage={ storage }
                                    updateList={ doUpdate }
                                    snesLoaded={ snesLoaded }/>)
};  

const ListWrapper = (props) => {
  let { onDrop, classes } = props;
  return (
    <Dropzone onDrop={ onDrop }
              style={ { minHeight: '200px' } }
              disableClick={ true }>
      { ({ open }) => (
        <>
          <Typography variant="h4" component="h4">ROMs</Typography>
          <RomList { ...props }/>
          <Button variant="contained" size="medium" className={ classes.addButton }
                  color="primary" onClick={ open }>Add ROM</Button>
        </>
      )}
    </Dropzone>
  )
};

export default enhanceList(ListWrapper);
