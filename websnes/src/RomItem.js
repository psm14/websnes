import React from 'react';
import { withHandlers, withState, compose } from 'recompose';
import { withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

const styles = (theme) => ({
  content: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'start',
    padding: 0,
  },
  actions: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'start',
    paddingBottom: theme.spacing.unit,
  },
  title: {
    flex: '1',
    padding: theme.spacing.unit * 3,
  },
  more: {
    width: theme.spacing.unit * 6,
    height: theme.spacing.unit * 6,
    marginTop: theme.spacing.unit,
    marginRight: theme.spacing.unit / 2,
  }
});

// Rom Item

const storageAction = (action) => ({ rom: { name }, storage, updateList, setMenuOpen }) => async () => {
  setMenuOpen(false);
  if (storage) {
    await storage[action](name);
  }
  await updateList();
}

const enhanceItem = compose(
  withStyles(styles),
  withState('menuOpen', 'setMenuOpen', null),
  withHandlers({
    onClickPlay: ({ rom: { name }, onPlay }) => () => {
      if (onPlay) {
        onPlay(name);
      }
    },
    onClickSRAM: storageAction('clearSRAM'),
    onClickDelete: storageAction('deleteRom'),
    openMenu: ({ setMenuOpen }) => (e) => setMenuOpen(e.currentTarget),
    closeMenu: ({ setMenuOpen }) => () => setMenuOpen(null),
  }),
)

const MoreMenu = ({ menuOpen, closeMenu, sram, onClickSRAM, onClickDelete }) => {
  let sramItem = sram ? (<MenuItem onClick={ onClickSRAM }>Clear SRAM</MenuItem>) : null;
  return (
    <Menu anchorEl={ menuOpen }
          open={ Boolean(menuOpen) }
          onClose={ closeMenu }>
      { sramItem }
      <MenuItem onClick={ onClickDelete }>Delete</MenuItem>
    </Menu>
  );
}

const RomItem = ({ rom: { name, sram }, onClickPlay, onClickSRAM, onClickDelete, classes,
                   menuOpen, openMenu, closeMenu, snesLoaded }) => {
  return (
    <div className={ classes.content }>
      <Typography variant="h6" component="div" className={classes.title}>
        { name }
      </Typography>
      <div className={ classes.actions }>
        <Button size="small" color="primary" disabled={ !snesLoaded }
                onClick={ onClickPlay }>Play</Button>
        <Button size="medium" onClick={ openMenu }>More</Button>
      </div>
      <MoreMenu menuOpen={ menuOpen } closeMenu={ closeMenu }
                sram={ sram } onClickSRAM={ onClickSRAM } onClickDelete={ onClickDelete }/>
      <Divider/>
    </div>
  );
}

export default enhanceItem(RomItem);
