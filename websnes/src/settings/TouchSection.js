import React from 'react';
import { compose, withPropsOnChange } from 'recompose';
import withSettingsHandlers from './with-settings-handlers';
import { ToggleSection } from './Section';
import NumberSetting from './NumberSetting';

const enhance = compose(
  withSettingsHandlers('touch', 'setTouchSetting', 'touch'),
  withPropsOnChange(['setTouchSetting'], ({ setTouchSetting }) => ({
    setEnabled: setTouchSetting('enabled'),
    setWidth: setTouchSetting('width'),
    setSideMargin: setTouchSetting('sideMargin'),
    setBottomMargin: setTouchSetting('bottomMargin'),
    setDeadZoneRadius: setTouchSetting('deadZoneRadius'),
    setInactiveOpacity: setTouchSetting('inactiveOpacity'),
    setActiveOpacity: setTouchSetting('activeOpacity'),
  })),
);

const TouchSection = (props) => {
  let { touch, classes } = props;
  let enabled = touch.enabled;
  return (
    <ToggleSection classes={ classes } name="Touch Input"
                  enabled={ enabled } setEnabled={ props.setEnabled }>
      <NumberSetting classes={ classes } enabled={ enabled }
         value={ touch.width } setValue={ props.setWidth }
         label="Width"/>
      <NumberSetting classes={ classes } enabled={ enabled }
         value={ touch.sideMargin } setValue={ props.setSideMargin }
         label="Side Margin"/>
      <NumberSetting classes={ classes } enabled={ enabled }
         value={ touch.bottomMargin } setValue={ props.setBottomMargin }
         label="Bottom Margin"/>
      <NumberSetting classes={ classes } enabled={ enabled }
         value={ touch.deadZoneRadius } setValue={ props.setDeadZoneRadius }
         label="Dead Zone Radius"/>
      <NumberSetting classes={ classes } enabled={ enabled }
         value={ touch.inactiveOpacity } setValue={ props.setInactiveOpacity }
         label="Inactive Opacity"/>
      <NumberSetting classes={ classes } enabled={ enabled }
         value={ touch.activeOpacity } setValue={ props.setActiveOpacity }
         label="Active Opacity"/>
    </ToggleSection>
  );
}

export default enhance(TouchSection);
