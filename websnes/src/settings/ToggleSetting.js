import React from 'react';
import Switch from '@material-ui/core/Switch';
import { withHandlers } from 'recompose';

const enhanceSwitch = withHandlers({
  onChange: ({ setValue }) => (e) => setValue(e.target.checked),
});

const ToggleSetting = ({ value, onChange, label }) => (
  <Switch
    checked={ value }
    onChange={ onChange }
    value={ label }
    color="primary"/>
);

export default enhanceSwitch(ToggleSetting);
