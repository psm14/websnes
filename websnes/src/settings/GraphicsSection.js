import React from 'react';
import { compose, withPropsOnChange } from 'recompose';
import withSettingsHandlers from './with-settings-handlers';
import SelectSetting from './SelectSetting';
import { Section } from './Section';
import VIDEO_MODES from '../lib/video-modes';


const enhance = compose(
  withSettingsHandlers('graphics', 'setGraphicsSetting', 'graphics'),
  withPropsOnChange(['setGraphicsSetting'], ({ setGraphicsSetting }) => ({
    setFilter: setGraphicsSetting('filter'),
  })),
);

const GraphicsSection = ({ classes, graphics, setFilter }) => (
  <Section classes={ classes } name="Graphics">
    <SelectSetting classes={ classes } label="Filter" enabled={ true }
                  value={ graphics.filter }
                  setValue={ setFilter }
                  options={ VIDEO_MODES }/>
  </Section>
);

export default enhance(GraphicsSection);
