import React from 'react';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import { compose } from 'recompose';
import GraphicsSection from './GraphicsSection';
import TouchSection from './TouchSection';
import KeySection from './KeySection';
import GamepadSection from './GamepadSection';

const styles = (theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
  },
  sectionTitle: {
    flexGrow: 1,
  },
  sectionHeader: {
    marginBottom: theme.spacing.unit * 2,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  section: {
    padding: theme.spacing.unit * 3,
    display: 'flex',
    flexDirection: 'column',
  },
  sectionBody: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  settingRow: {
    display: 'flex',
    flexDirection: 'row',
  },
  textField: {
    width: 200,
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
  },
  keybind: {
    display: 'flex',
    flexDirection: 'column',
    width: 200,
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit  * 3,
    marginTop: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit,
  },
});

const SECTIONS = [GraphicsSection, TouchSection, KeySection, GamepadSection];

const enhance = compose(
  withStyles(styles),
);

const Settings = ({ classes, settings, setSettings }) => {
  return (
    <div className={ classes.container }>
      <Typography variant="h4" component="h4">
        Settings
      </Typography>
      { SECTIONS.map((Section, idx) => (
        <Section key={ idx } classes={ classes } settings={ settings }
                 setSettings={ setSettings }/>
      ))}
    </div>
  );
}

export default enhance(Settings);
