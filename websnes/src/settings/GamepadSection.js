import React from 'react';
import { compose, withState, withHandlers } from 'recompose';
import PadSetting from './PadSetting';
import { ToggleSection } from './Section';
import withSettingsHandlers from './with-settings-handlers';
import PadBindManager from './PadBindManager';

const enhance = compose(
  withSettingsHandlers('gamepad', 'setPadSetting', 'gamepad'),
  withHandlers({
    getPadSetting: ({ gamepad }) => (name) => gamepad[name],
    setEnabled: ({ setPadSetting }) => setPadSetting('enabled'),
  }),
  withState('active', 'setActive', null),
);

const GamepadSection = ({ classes, gamepad: { enabled }, getPadSetting, setPadSetting,
                          active, setActive, setEnabled }) => {
  let Binder = ({ padProp, label }) => (
    <PadSetting classes={ classes } enabled={ enabled } getPadSetting={ getPadSetting }
                active={ active } setActive={ setActive }
                padProp={ padProp } label={ label }/>
  )
  return (
    <ToggleSection classes={ classes } name="Gamepad Input"
                  enabled={ enabled } setEnabled={ setEnabled }>
      <Binder padProp="key_left" label="Left"/>
      <Binder padProp="key_up" label="Up"/>
      <Binder padProp="key_right" label="Right"/>
      <Binder padProp="key_down" label="Down"/>
      <Binder padProp="key_select" label="Select"/>
      <Binder padProp="key_start" label="Start"/>
      <Binder padProp="key_b" label="B"/>
      <Binder padProp="key_a" label="A"/>
      <Binder padProp="key_y" label="Y"/>
      <Binder padProp="key_x" label="X"/>
      <Binder padProp="key_l" label="L"/>
      <Binder padProp="key_r" label="R"/>
      <PadBindManager active={ active } setActive={ setActive }
                      setPadSetting={ setPadSetting }/>
    </ToggleSection>
  )
};

export default enhance(GamepadSection);
