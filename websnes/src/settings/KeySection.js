import React from 'react';
import { compose, withState, withHandlers } from 'recompose';
import KeySetting from './KeySetting';
import { ToggleSection } from './Section';
import withSettingsHandlers from './with-settings-handlers';
import BindManager from './BindManager';

const enhance = compose(
  withSettingsHandlers('keyboard', 'setKeySetting', 'keyboard'),
  withHandlers({
    getKeySetting: ({ keyboard }) => (name) => keyboard[name],
    setEnabled: ({ setKeySetting }) => setKeySetting('enabled'),
  }),
  withState('active', 'setActive', null),
);

const KeySection = ({ classes, keyboard: { enabled }, getKeySetting, setKeySetting,
                                 active, setActive, setEnabled }) => {
  let Binder = ({ keyProp, label }) => (
    <KeySetting classes={ classes } enabled={ enabled } getKeySetting={ getKeySetting }
                active={ active } setActive={ setActive }
                keyProp={ keyProp } label={ label }/>
  )
  return (
    <ToggleSection classes={ classes } name="Keyboard Input"
                  enabled={ enabled } setEnabled={ setEnabled }>
      <Binder keyProp="key_left" label="Left"/>
      <Binder keyProp="key_up" label="Up"/>
      <Binder keyProp="key_right" label="Right"/>
      <Binder keyProp="key_down" label="Down"/>
      <Binder keyProp="key_select" label="Select"/>
      <Binder keyProp="key_start" label="Start"/>
      <Binder keyProp="key_b" label="B"/>
      <Binder keyProp="key_a" label="A"/>
      <Binder keyProp="key_y" label="Y"/>
      <Binder keyProp="key_x" label="X"/>
      <Binder keyProp="key_l" label="L"/>
      <Binder keyProp="key_r" label="R"/>
      <BindManager active={ active } setActive={ setActive }
                   setKeySetting={ setKeySetting }/>
    </ToggleSection>
  )
};

export default enhance(KeySection);
