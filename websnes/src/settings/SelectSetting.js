import React from 'react';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import { withHandlers } from 'recompose';

const enhance = withHandlers({
  onChange: ({ setValue }) => (e) => setValue(e.target.value),
});

const SelectSetting = ({ classes, options, value, onChange,
                                       label, enabled }) => (
  <TextField
      select
      className={ classes.textField }
      disabled={ !enabled }
      label={ label }
      value={ value }
      onChange={ onChange }
      margin="normal">
    {options.map(option => (
      <MenuItem key={option.value} value={option.value}>
        {option.name}
      </MenuItem>
    ))}
  </TextField>
);

export default enhance(SelectSetting);
