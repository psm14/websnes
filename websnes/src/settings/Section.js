import React from 'react';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import ToggleSetting from './ToggleSetting';

const Section = ({ classes, name, children }) => (
  <>
  <div className={ classes.section }>
    <div className={ classes.sectionHeader }>
      <Typography variant="h5" component="h5" className={ classes.sectionTitle }>
        { name }
      </Typography> 
    </div>
    <div className={ classes.sectionBody }>
      { children }
    </div>
  </div>
  <Divider/>
  </>
);

const ToggleSection = ({ classes, name, children, enabled, setEnabled }) => (
  <>
  <div className={ classes.section }>
    <div className={ classes.sectionHeader }>
      <Typography variant="h5" component="h5" className={ classes.sectionTitle }>
        { name }
      </Typography> 
      <ToggleSetting value={ enabled } setValue={ setEnabled }
                     label="enabled"/>
    </div>
    <div className={ classes.sectionBody }>
      { children }
    </div>
  </div>
  <Divider/>
  </>
);

export { Section, ToggleSection };
