import { branch, compose, withPropsOnChange } from 'recompose';
import { withLooper, DummyElement } from '../lib/recompose-extras';

const pollGamepad = ({ setActive, active, setPadSetting }) => {
  if (!navigator.getGamepads) {
    return;
  }
  let gamepad = navigator.getGamepads()[0];
  if (!gamepad) {
    return;
  }
  for (let i = 0; i < gamepad.buttons.length; i++) {
    let button = gamepad.buttons[i];
    if (button.pressed) {
      setPadSetting(active)(i);
      setActive(null);
      break;
    }
  }
};

const withButtonListener = compose(
  withPropsOnChange([], () => ({
    buttonListener: pollGamepad,
  })),
  withLooper('buttonListener'),
)

const enhance = branch(
  ({ active }) => active !== null,
  withButtonListener,
);


export default enhance(DummyElement);
