import React from 'react';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import { withHandlers, compose, withPropsOnChange } from 'recompose';

// There is a lot of code duplication here, but it could diverge greatly
// with special button labels and stuff

const enhance = compose(
  withPropsOnChange(['getPadSetting', 'active', 'padProp'], ({ getPadSetting, active, padProp }) => ({
    isActive: active === padProp,
    padName: 'Button ' + getPadSetting(padProp),
  })),
  withHandlers({
    onClick: ({ isActive, setActive, padProp }) => (e) => {
      setActive(isActive ? null : padProp);
    },
  }),
);

const PadSetting = ({ label, padName, onClick, enabled,
                      classes, isActive }) => (
  <div className={ classes.keybind }>
    <InputLabel shrink={ true } disabled={ !enabled }
                focused={ isActive }>
      { label }
    </InputLabel>
    <Button variant="outlined" onClick={ onClick } disabled={ !enabled }
            color={ isActive ? "primary" : null }>
      { isActive ? '...' : padName }
    </Button>
  </div>
);

export default enhance(PadSetting);
