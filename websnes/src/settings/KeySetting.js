import React from 'react';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import { withHandlers, compose, withPropsOnChange } from 'recompose';
import keyCode from 'keycode';

const enhance = compose(
  withPropsOnChange(['getKeySetting', 'active', 'keyProp'], ({ getKeySetting, active, keyProp }) => ({
    isActive: active === keyProp,
    keyName: keyCode(getKeySetting(keyProp)),
  })),
  withHandlers({
    onClick: ({ isActive, setActive, keyProp }) => (e) => {
      setActive(isActive ? null : keyProp);
    },
  }),
);

const KeySetting = ({ label, keyName, onClick, enabled,
                                        classes, isActive }) => (
  <div className={ classes.keybind }>
    <InputLabel shrink={ true } disabled={ !enabled }
               focused={ isActive }>
      { label }
    </InputLabel>
    <Button variant="outlined" onClick={ onClick } disabled={ !enabled }
            color={ isActive ? "primary" : null }>
      { isActive ? '...' : keyName }
    </Button>
  </div>
);

export default enhance(KeySetting);
