import { branch } from 'recompose';
import { withEventListener, DummyElement } from '../lib/recompose-extras';

const enhance = branch(
  ({ active }) => active !== null,
  withEventListener(window, 'keydown', ({ setActive, active, setKeySetting, event }) => {
    setKeySetting(active)(event.keyCode);
    setActive(null);
    event.preventDefault();
  }),
);

export default enhance(DummyElement);
