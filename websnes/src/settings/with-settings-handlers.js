import { compose, withPropsOnChange } from 'recompose';

const withSettingsHandlers = (propName, setterName, property) => compose(
  withPropsOnChange(['settings', 'setSettings'], ({ settings, setSettings }) => ({
    [propName]: settings[property],
    [setterName]: (name) => (value) => {
      setSettings({
        ...settings,
        [property]: {
          ...settings[property],
          [name]: value,
        },
      });
    },
  })),
);

export default withSettingsHandlers;
