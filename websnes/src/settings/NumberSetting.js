import React from 'react';
import TextField from '@material-ui/core/TextField';
import { withHandlers, compose, withState, withPropsOnChange } from 'recompose';

const enhance = compose(
  withState('textValue', 'setTextValue', ({ value }) => `${value}`),
  withHandlers({
    onChange: ({ setTextValue }) => (e) => setTextValue(e.target.value),
  }),
  withPropsOnChange(['textValue', 'setValue', 'value'], ({ textValue, setValue, value }) => {
    let parsed = Number(textValue);
    // TODO: Better check between 0 and an invalid value
    let valid = Boolean(parsed) || textValue === '0';
    if (valid && value !== parsed) {
      setValue(parsed);
    }
    return {
      valid
    };
  })
);

const NumberSetting = ({ textValue, valid, onChange,
                         label, enabled, classes }) => (
  <TextField
    className={ classes.textField }
    disabled={ !enabled }
    error={ !valid }
    label={ label }
    value={ textValue }
    onChange={ onChange }
    InputLabelProps={{
      shrink: true,
    }}
    margin="normal"
  />
);

export default enhance(NumberSetting);
