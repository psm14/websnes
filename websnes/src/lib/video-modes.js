const VIDEO_MODES = [
  { value: 1, name: "Blocky" },
  { value: 2, name: "TV" },
  { value: 3, name: "Smooth" },
  { value: 4, name: "Super Eagle" },
  { value: 5, name: "2xSaI" },
  { value: 6, name: "Super 2xSaI" },
  { value: 7, name: "EPX" },
  { value: 8, name: "HQ2X" },
];

export default VIDEO_MODES;
