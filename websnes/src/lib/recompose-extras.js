import React from 'react';
import { lifecycle, withPropsOnChange } from 'recompose';

const DummyElement = () => null;

const withLens = (valueName, setterName, property, mappedName, mappedSetterName) => {
  return withPropsOnChange([valueName, setterName], (props) => ({
    [mappedName]: props[valueName][property],
    [mappedSetterName]: (value) => {
      let mappedValue = {
        ...props[valueName],
        [property]: value,
      };
      return props[setterName](mappedValue);
    },
  }));
}

const withRef = (name) => withPropsOnChange([], () => ({
  [name]: React.createRef(),
}));

const withLooper = (loopProp) => lifecycle({
  componentDidMount() {
    this._loop = true;
    let looper = () => {
      if (this._loop) {
        requestAnimationFrame(looper);
        let loop = this.props[loopProp];
        if (loop) {
          loop(this.props);
        }
      }
    };
    requestAnimationFrame(looper);
  },
  componentWillUnmount() {
    this._loop = false;
  },
});

const withMountAction = (mountAction) => lifecycle({
  componentDidMount() {
    let unmountAction = mountAction(this.props);
    if (unmountAction && typeof unmountAction === 'function') {
      this._unmountAction = unmountAction;
    }
  },
  componentWillUnmount() {
    if (this._unmountAction) {
      this._unmountAction(this.props);
    }
  }
});

const withAsyncMountAction = (asyncAction) => lifecycle({
  async componentDidMount() {
    this._unmounted = false;
    let unmountAction = await asyncAction(this.props);
    // Check if we are already unmounted now
    if (this._unmounted) {
      // If so, execute immediately
      unmountAction(this.props);
    } else {
      this._unmountAction = unmountAction;
    }
  },
  componentWillUnmount() {
    if (this._unmountAction) {
      this._unmountAction(this.props);
    } else {
      this._unmounted = true;
    }
  },
})

const withEventListener = (target, actions, listener) => lifecycle({
  componentDidMount() {
    if (typeof listener === 'string') {
      let listenerProp = listener;
      listener = (props) => { props[listenerProp](props) };
    }
    this._wrapped = (event) => listener({ ...this.props, event });
    if (typeof target === 'string') {
      target = this.props[target];
    } else if (typeof target === 'function' && !target.addEventListener) {
      target = target(this.props);
    }
    this._target = target;
    if (!Array.isArray(actions)) {
      actions = [ actions ];
    }
    actions.forEach(action => {
      this._target.addEventListener(action, this._wrapped);
    });
  },
  componentWillUnmount() {
    if (!Array.isArray(actions)) {
      actions = [ actions ];
    }
    actions.forEach(action => {
      this._target.removeEventListener(action, this._wrapped);
    });
  },
});

const withPropChangeHandler = (onChange) => lifecycle({
  componentDidMount() {
    onChange({}, this.props);
  },
  componentDidUpdate(prevProps) {
    onChange(prevProps, this.props);
  },
  componentWillUnmount() {
    onChange(this.props, {});
  }
});

const connectPropToSetter = (setterName, propName) => withPropChangeHandler((prev, props) => {
  let prevSetter = prev[setterName];
  let prevProp = prev[propName];

  let setter = props[setterName];
  let prop = props[propName];
  if (setter && setter !== prevSetter && prop !== prevProp) {
    setter(prop);
  }
})

export { withLooper, withMountAction, withAsyncMountAction, withEventListener,
         withPropChangeHandler, connectPropToSetter, DummyElement, withRef,
         withLens };
