import indexedDBP from 'indexed-db-as-promised';
import { compose, withState } from 'recompose';
import { withAsyncMountAction } from './recompose-extras';

let migrate = (db, { transaction, oldVersion, newVersion }) => {
  if (oldVersion <= 0) {
    const romStore = db.createObjectStore('roms', {keyPath: 'id', autoIncrement: true});
    romStore.createIndex('name', 'name', { unique: true });
  }

  if (oldVersion <= 1) {
    db.deleteObjectStore('roms');
    db.createObjectStore('roms', {keyPath: 'name'});
  }
}

let openDb = async () => {
  let db = await indexedDBP.open('roms', 2, { upgrade: migrate });
  return new Storage(db);
}

class Storage {
  constructor(db) {
    this.db = db;
    window.thedb = db;
  }

  async getRom(name) {
    return await this.db.transaction('roms').run((tx) => {
      return tx.objectStore('roms').get(name);
    });
  }

  async addRom(name, buffer) {
    let item = {
      name,
      rom: buffer
    };
    await this.db.transaction('roms', 'readwrite').run((tx) => {
      return tx.objectStore('roms').add(item);
    })
    return item;
  }

  async deleteRom(name) {
    return await this.db.transaction('roms', 'readwrite').run((tx) => {
      return tx.objectStore('roms').delete(name);
    });
  }

  async listRoms() {
    return await this.db.transaction('roms').run((tx) => {
      return tx.objectStore('roms').openCursor().iterate((cursor) => {
        let record = cursor.value;
        cursor.continue();
        return { name: record.name, sram: Boolean(record.sram) };
      });
    });
  }

  async saveSRAM(name, sram) {
    return await this.db.transaction('roms', 'readwrite').run(async (tx) => {
      let store = tx.objectStore('roms');
      let record = await store.get(name);
      if (record) {
        return store.put({ ...record, sram });
      }
    });
  }

  async clearSRAM(name) {
    await this.saveSRAM(name, null);
  }

  close() {
    this.db.close();
  }
}

const withStorageConnection = (onConnect) => compose(
  withState('storage', 'setStorage', null),
  withAsyncMountAction(async (props) => {
    let { setStorage } = props;
    let storage = await openDb();
    setStorage(storage);
    if (onConnect) {
      onConnect({ ...props, storage });
    }

    return () => {
      storage.close();
    }
  }),
);

export { openDb, withStorageConnection };
