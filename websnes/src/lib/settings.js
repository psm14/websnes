import isTouchDevice from 'is-touch-device';

const DEFAULT_TOUCH_SETTINGS = {
  enabled: isTouchDevice(),
  width: 160,
  sideMargin: 10,
  bottomMargin: 10,
  deadZoneRadius: 20,
  buttonAngle: 3 * Math.PI / 4,
  inactiveOpacity: 0.4,
  activeOpacity: 0.75,
  buttonMap: {
    left: {
      shoulder: 10,
      left: 1,
      top: 3,
      right: 0,
      bottom: 2,
      control: 5,
    },
    right: {
      shoulder: 11,
      left: 9,
      top: 8,
      right: 6,
      bottom: 7,
      control: 4,
    },
  },
};

const DEFAULT_GRAPHICS_SETTINGS = {
  filter: 1, // Blocky
}

const DEFAULT_KEYBOARD_SETTINGS = {
  enabled: true,
  key_l: 81,
  key_left: 37,
  key_up: 38,
  key_right: 39,
  key_down: 40,
  key_select: 222,
  key_r: 87,
  key_y: 83,
  key_x: 65,
  key_a: 88,
  key_b: 90,
  key_start: 13,
}

const DEFAULT_GAMEPAD_SETTINGS = {
  enabled: true,
  key_l: 8,
  key_left: 2,
  key_up: 0,
  key_right: 3,
  key_down: 1,
  key_select: 5,
  key_r: 9,
  key_y: 13,
  key_x: 14,
  key_a: 12,
  key_b: 11,
  key_start: 4,
}

export { DEFAULT_TOUCH_SETTINGS, DEFAULT_GRAPHICS_SETTINGS,
         DEFAULT_KEYBOARD_SETTINGS, DEFAULT_GAMEPAD_SETTINGS }
