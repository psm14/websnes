import React from 'react';
import { compose, defaultProps, withPropsOnChange } from 'recompose';
import { withEventListener, DummyElement } from '../recompose-extras';
import { DEFAULT_KEYBOARD_SETTINGS } from '../settings';

/*
#define BTN_RIGHT  0
#define BTN_LEFT   1
#define BTN_DOWN   2
#define BTN_UP     3
#define BTN_START  4
#define BTN_SELECT 5
#define BTN_A      6
#define BTN_B      7
#define BTN_X      8
#define BTN_Y      9
#define BTN_L      10
#define BTN_R      11*/

let keyHandler = (isDown) => ({ keymap, bouncer, event }) => {
  let keyIdx = keymap.indexOf(event.keyCode);
  if (keyIdx > -1 && bouncer) {
    bouncer.sendButton(keyIdx, isDown);
    event.preventDefault();
  }
}

const enhance = compose(
  defaultProps({ settings: DEFAULT_KEYBOARD_SETTINGS }),
  withPropsOnChange(['settings'], ({ settings }) => ({
    keymap: [
      settings.key_right, settings.key_left, settings.key_down,
      settings.key_up, settings.key_start, settings.key_select, settings.key_a,
      settings.key_b, settings.key_x, settings.key_y, settings.key_l,
      settings.key_r,
    ],
  })),
  withEventListener(document, 'keydown', keyHandler(true)),
  withEventListener(document, 'keyup', keyHandler(false)),
);

const KeyInput = enhance(DummyElement);

export default (props) => props.enabled ? <KeyInput { ...props }/> : null;
