class InputManager {
  constructor() {
    this._pollFns = {};
    this._pollArray = [];
    this._sendButton = null;
  }
  
  setSendButton(fn) {
    this._sendButton = fn;
  }

  sendButton(...args) {
    if (this._sendButton) {
      this._sendButton(...args);
    }
  }

  registerPollFn(key, fn) {
    this._pollFns[key] = fn;
    this.refreshPollArray();
  }

  removePollFn(key) {
    delete this._pollFns[key];
    this.refreshPollArray();
  }

  refreshPollArray() {
    this._pollArray = Object.values(this._pollFns);
  }

  poll() {
    for (let i = 0; i < this._pollArray.length; i++) {
      this._pollArray[i]();
    }
  }
}

export default InputManager;
