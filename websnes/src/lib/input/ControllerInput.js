import React from 'react';
import { compose, defaultProps, withHandlers,
         withPropsOnChange } from 'recompose';
import { withPropChangeHandler, DummyElement } from '../recompose-extras';
import { DEFAULT_GAMEPAD_SETTINGS } from '../settings';

/*
#define BTN_RIGHT  0
#define BTN_LEFT   1
#define BTN_DOWN   2
#define BTN_UP     3
#define BTN_START  4
#define BTN_SELECT 5
#define BTN_A      6
#define BTN_B      7
#define BTN_X      8
#define BTN_Y      9
#define BTN_L      10
#define BTN_R      11*/

const enhance = compose(
  defaultProps({ settings: DEFAULT_GAMEPAD_SETTINGS }),
  withPropsOnChange(['settings'], ({ settings }) => ({
    keymap: [
      settings.key_right, settings.key_left, settings.key_down,
      settings.key_up, settings.key_start, settings.key_select, settings.key_a,
      settings.key_b, settings.key_x, settings.key_y, settings.key_l,
      settings.key_r,
    ],
  })),
  withHandlers({
    updateGamepad: ({ setGamepad }) => () => {
      let gamepad = navigator.getGamepads()[0] || null;
      setGamepad(gamepad);
    },
    pollGamepad: ({ keymap, bouncer }) => () => {
      if (!navigator.getGamepads) {
        return;
      }
      let gamepad = navigator.getGamepads()[0];
      if (!gamepad) {
        return;
      }
      for (let snesButton = 0; snesButton < keymap.length; snesButton++) {
        let gamepadIdx = keymap[snesButton];
        let gamepadButton = gamepad.buttons[gamepadIdx];
        let pressed = gamepadButton && gamepadButton.pressed;
        bouncer.sendButton(snesButton, pressed);
      }
    }
  }),
  withPropChangeHandler(({ bouncer: prevBouncer },
                         { bouncer, pollGamepad }) => {
    if (prevBouncer) {
      prevBouncer.removePollFn('gamepad');
    }
    if (bouncer) {
      bouncer.registerPollFn('gamepad', pollGamepad);
    }
  }),
)

const GamepadInput = enhance(DummyElement);

export default (props) => props.enabled ? <GamepadInput { ...props }/> : null;
