import React from 'react';
import { withPropsOnChange, defaultProps, withState, withHandlers, compose } from 'recompose';
import { withRef } from './lib/recompose-extras';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  container: {
    position: 'absolute',
  },
  button: {
    display: 'inline-block',
    position: 'absolute',
    borderStyle: 'solid',
    borderColor: '#666666',
    borderWidth: '4px',
    color: '#666666',
    fontWeight: 800,
    fontFamily: 'Arial, Helvetica, sans-serif',
    textAlign: 'center',
    verticalAlign: 'middle',
  },
};

const heightFromWidth = (width) => width * 25 / 18;

const containerLeftRightStyle = (isLeft, sideMargin) => (
  isLeft ? { left: `${sideMargin}px` } : { right: `${sideMargin}px` }
);

const containerStyle = (isLeft, width, sideMargin, bottomMargin) => {
  let height = heightFromWidth(width);
  return Object.assign(
    {},
    {
      width: `${width}px`,
      height: `${height}px`,
      bottom: `${bottomMargin}px`,
    },
    containerLeftRightStyle(isLeft, sideMargin),
  );
}

const textSize = (buttonRadius) => {
  let size = Math.floor(buttonRadius);
  return {
    fontSize: `${size}px`,
    lineHeight: `${buttonRadius * 2 - 8}px`,
  };
}

const buttonSize = (buttonRadius) => {
  let dim = `${buttonRadius * 2}px`;
  return {
    width: dim,
    height: dim,
    borderRadius: `${buttonRadius}px`,
  };
}

const buttonPosition = (row, col, width) => {
  let gridSize = width / 3;
  let height = heightFromWidth(width);
  let topInset = height - width;
  return {
    top: `${(row * gridSize) + topInset}px`,
    left: `${col * gridSize}px`,
  };
}

const buttonStyle = (row, col, width) => {
  let buttonRadius = width / 6;
  return Object.assign(
    {},
    textSize(buttonRadius),
    buttonSize(buttonRadius),
    buttonPosition(row, col, width),
  );
}

const getShoulderOffset = (isLeft, width) => isLeft ? 0 : width - shoulderWidthFromWidth(width);

const shoulderPosition = (isLeft, width) => ({
  left: `${getShoulderOffset(isLeft, width)}px`,
  top: '0px',
});

const controlHeightFromWidth = (width) => width / 6;
const shoulderWidthFromWidth = (width) => width * 10 / 18;

const shoulderStyle = (isLeft, width) => {
  let height = controlHeightFromWidth(width);
  let shoulderWidth = shoulderWidthFromWidth(width);
  return Object.assign(
    {},
    textSize(height / 2),
    {
      width: `${shoulderWidth}px`,
      height: `${height}px`,
      borderRadius: `${height/2}px`,
    },
    shoulderPosition(isLeft, width),
  )
}

const controlWidthFromWidth = (width) => width * 4 / 18;
const getControlMargin = (width) => width * 5 / 180;
const getControlOffset = (isLeft, width) => isLeft ? width - controlWidthFromWidth(width) - getControlMargin(width) : getControlMargin(width);

const controlPosition = (isLeft, width) => ({
  left: `${getControlOffset(isLeft, width)}px`,
  top: '0px',
});

const controlStyle = (isLeft, width) => {
  let height = controlHeightFromWidth(width);
  let controlWidth = width * 4 / 18;
  return Object.assign(
    {},
    textSize(height / 2),
    {
      width: `${controlWidth}px`,
      height: `${height}px`,
      borderRadius: `${height/2}px`,
    },
    controlPosition(isLeft, width),
  )
}

const syncButtons = (bouncer, buttons, hits) => {
  if (bouncer) {
    bouncer.sendButton(buttons.top, hits.top);
    bouncer.sendButton(buttons.left, hits.left);
    bouncer.sendButton(buttons.right, hits.right);
    bouncer.sendButton(buttons.bottom, hits.bottom);
    bouncer.sendButton(buttons.shoulder, hits.shoulder);
    bouncer.sendButton(buttons.control, hits.control);
  }
}

class TouchIntersector {
  constructor(isLeft, settings) {
    this.controlStart = getControlOffset(isLeft, settings.width);
    let controlWidth = controlWidthFromWidth(settings.width);
    this.controlEnd = controlWidth + this.controlStart;
    this.shoulderStart = getShoulderOffset(isLeft, settings.width);
    let shoulderWidth = shoulderWidthFromWidth(settings.width);
    this.shoulderEnd = shoulderWidth + this.shoulderStart;

    this.width = settings.width;
    this.topInset = heightFromWidth(settings.width) - settings.width;
    this.controlHeight = controlHeightFromWidth(settings.width);
    this.insideRadius = settings.deadZoneRadius;
    this.buttonAngleSize = settings.buttonAngle;

    this.centerX = this.width / 2;
    this.centerY = this.topInset + this.centerX;
  }

  intersectTop(start, end, touch) {
    if (this.isLeft) {
      let tmp = start;
      start = this.width - end;
      end = this.width - tmp;
    }
    return touch.x >= start &&
           touch.x <= end &&
           touch.y >= 0 && 
           touch.y <= this.controlHeight;
  }
  
  calcDistAndRadius(touch) {
    let dx = touch.x - this.centerX;
    let dy = touch.y - this.centerY;
    let dist = Math.sqrt((dx * dx) + (dy * dy));
    return dist;
  }
  
  containedInZone(touch, zoneRadius) {
    let dist = this.calcDistAndRadius(touch);  
    return dist <= zoneRadius;
  }

  inTouchZone(touch) {
    return !this.containedInZone(touch, this.insideRadius) && this.containedInZone(touch, this.width / 2);
  }

  touchAngle(touch) {
    let dx = touch.x - this.centerX;
    let dy = touch.y - this.centerY;
    return Math.atan2(-dy, dx);    
  }

  hitsButton(angle, buttonAngle) {
    let max = buttonAngle + this.buttonAngleSize / 2;
    let min = buttonAngle - this.buttonAngleSize / 2;
    return angle >= min && angle <= max;
  }

  intersect(hits, touch) {
    hits.control |= this.intersectTop(this.controlStart, this.controlEnd, touch);
    hits.shoulder |= this.intersectTop(this.shoulderStart, this.shoulderEnd, touch);

    if (!this.inTouchZone(touch)) {
      return;
    }

    let angle = this.touchAngle(touch);
    hits.right |= this.hitsButton(angle, 0);
    hits.top |= this.hitsButton(angle, Math.PI / 2);
    hits.left |= this.hitsButton(angle, Math.PI);
    hits.left |= this.hitsButton(angle, -Math.PI);
    hits.bottom |= this.hitsButton(angle, -Math.PI / 2);
  }
}

const shouldIgnoreTouch = (e, touch) => {
  for (let i = 0; i < e.changedTouches.length; i++) {
    let changed = e.changedTouches[i];
    return touch.identifier === changed.identifier;
  }
}

const isTouchDown = (e) => {
  return e.type === "touchstart" || e.type === "touchmove";
}

const touchHandler = ({ bouncer, settings, isLeft, setButtons, container,
                        intersector }) => (e) => {
  let buttonMap = isLeft ? settings.buttonMap.left : settings.buttonMap.right;
  let rect = container.current.getBoundingClientRect();

  let hits = {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    shoulder: 0,
    control: 0,
  };
  for (let i = 0; i < e.touches.length; i++) {
    let touch = e.touches[i];
    if (!isTouchDown(e) && shouldIgnoreTouch(e, touch)) {
      continue;
    }
    let coords = {
      x: touch.clientX - rect.left,
      y: touch.clientY - rect.top,
    };
    intersector.intersect(hits, coords);
  }
  syncButtons(bouncer, buttonMap, hits);
  setButtons(hits);
}

const LABELS = {
  left: {
    top: '∧',
    left: '<',
    right: '>',
    bottom: '∨',
    shoulder: 'L',
    control: 'Se',
  },
  right: {
    top: 'X',
    left: 'Y',
    right: 'A',
    bottom: 'B',
    shoulder: 'R',
    control: 'St',
  }
}

const enhance = compose(
  withStyles(styles),
  defaultProps({
    labels: LABELS,
  }),
  withRef('container'),
  withPropsOnChange(['isLeft', 'labels', 'settings'],
                    ({ isLeft, labels, settings }) => ({
    labels: isLeft ? labels.left : labels.right,
    styles: {
      top: buttonStyle(0, 1, settings.width),
      left: buttonStyle(1, 0, settings.width),
      right: buttonStyle(1, 2, settings.width),
      bottom: buttonStyle(2, 1, settings.width),
      control: controlStyle(isLeft, settings.width),
      shoulder: shoulderStyle(isLeft, settings.width),
      container: containerStyle(isLeft, settings.width, settings.sideMargin, settings.bottomMargin),
    },
    intersector: new TouchIntersector(isLeft, settings),
  })),
  withState('buttons', 'setButtons', {}),
  withHandlers({
    onTouch: touchHandler,
  }),
);

const TouchPad = ({ classes, styles, labels, onTouch, container, buttons, settings }) => {
  let buttonStyle = (style, active) => ({
    ...style,
    opacity: active ? settings.activeOpacity : settings.inactiveOpacity,
  });
  return (
    <div ref={ container } style={ styles.container } onTouchStart={ onTouch }
         onTouchEnd={ onTouch } onTouchMove={ onTouch } onTouchCancel={ onTouch }
         className={ classes.container }>
      <div style={ buttonStyle(styles.top, buttons.top) }
           className={ classes.button }>
        { labels.top }
      </div>
      <div style={ buttonStyle(styles.left, buttons.left) }
           className={ classes.button }>
        { labels.left }
      </div>
      <div style={ buttonStyle(styles.right, buttons.right) }
           className={ classes.button }>
        { labels.right }
      </div>
      <div style={ buttonStyle(styles.bottom, buttons.bottom) }
           className={ classes.button }>
        { labels.bottom  }
      </div>
      <div style={ buttonStyle(styles.shoulder, buttons.shoulder) }
           className={ classes.button }>
        { labels.shoulder }
      </div>
      <div style={ buttonStyle(styles.control, buttons.control) }
           className={ classes.button }>
        { labels.control }
      </div>
    </div>
  );
}

export default enhance(TouchPad);
