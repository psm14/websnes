#include <ctype.h>
#include <sys/types.h>
#include <emscripten.h>
#include "snes9x.h"
#include "port.h"
#include "controls.h"
#include "conffile.h"
#include "buttons.h"

static bool buttons_changed = false;
static bool button_states[NUM_BUTTONS];

EMSCRIPTEN_KEEPALIVE
extern "C" void report_button(int index, bool state) {
    if (index < NUM_BUTTONS) {
        button_states[index] = state;
        buttons_changed = true;
    }
}

void S9xParseInputConfig(ConfigFile &conf, int pass) {
	return;
}

void S9xInitInputDevices(void) {
	S9xUnmapAllControls();
    S9xMapButton(BTN_RIGHT, S9xGetCommandT("Joypad1 Right"), false);
    S9xMapButton(BTN_LEFT, S9xGetCommandT("Joypad1 Left"), false);
    S9xMapButton(BTN_DOWN, S9xGetCommandT("Joypad1 Down"), false);
    S9xMapButton(BTN_UP, S9xGetCommandT("Joypad1 Up"), false);
    S9xMapButton(BTN_START, S9xGetCommandT("Joypad1 Start"), false);
    S9xMapButton(BTN_SELECT,  S9xGetCommandT("Joypad1 Select"), false);
    S9xMapButton(BTN_A, S9xGetCommandT("Joypad1 A"), false);
    S9xMapButton(BTN_B, S9xGetCommandT("Joypad1 B"), false);
    S9xMapButton(BTN_X, S9xGetCommandT("Joypad1 X"), false);
    S9xMapButton(BTN_Y, S9xGetCommandT("Joypad1 Y"), false);
    S9xMapButton(BTN_L, S9xGetCommandT("Joypad1 L"), false);
    S9xMapButton(BTN_R, S9xGetCommandT("Joypad1 R"), false);

    for (int i = 0; i < NUM_BUTTONS; i++) {
        button_states[i] = false;
    }
}

void S9xProcessEvents(bool8 block) {
	bool8 quit_state = FALSE;

    if (buttons_changed) {
        for (int i = 0; i < NUM_BUTTONS; i++) {
            S9xReportButton(i, button_states[i]);
        }
        buttons_changed = false;
    }

	if (quit_state == TRUE)
	{
		printf ("Quit Event. Bye.\n");
		S9xExit();
	}
}

bool S9xPollButton(uint32 id, bool *pressed) {
	return false;
}

bool S9xPollAxis(uint32 id, int16 *value) {
	return false;
}

bool S9xPollPointer(uint32 id, int16 *x, int16 *y) {
	return false;
}

void S9xHandlePortCommand(s9xcommand_t cmd, int16 data1, int16 data2) {
	return;
}
