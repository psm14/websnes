#include <ctype.h>
#include <sys/types.h>
#include <emscripten.h>
#include "snes9x.h"
#include "ppu.h"
#include "conffile.h"
#include "display.h"
#include "blit.h"

#define GFX_SCALE 2

typedef	void (* Blitter) (uint8 *, int, uint8 *, int, int, int);

enum {
	VIDEOMODE_BLOCKY = 1,
	VIDEOMODE_TV,
	VIDEOMODE_SMOOTH,
	VIDEOMODE_SUPEREAGLE,
	VIDEOMODE_2XSAI,
	VIDEOMODE_SUPER2XSAI,
	VIDEOMODE_EPX,
	VIDEOMODE_HQ2X
};

static uint8 videoMode = VIDEOMODE_BLOCKY;

EMSCRIPTEN_KEEPALIVE
extern "C" uint8 get_video_mode() { 
	return videoMode;
}

EMSCRIPTEN_KEEPALIVE
extern "C" void set_video_mode(uint8 mode) {
	videoMode = mode;
}

void S9xExtraDisplayUsage(void) {
}

void S9xParseDisplayArg(char **argv, int &i, int argc) {
}

const char* S9xParseDisplayConfig(ConfigFile &conf, int pass) {
	return "Emscripten";
}

static uint8* inputFramebuffer;
static uint8* blitFramebuffer;
static uint8* outputFramebuffer;

void S9xInitDisplay(int argc, char **argv) {
	GFX.Pitch = SNES_WIDTH * 2 * GFX_SCALE;
	uint32 baseSize = GFX.Pitch * (SNES_HEIGHT_EXTENDED + 4) * GFX_SCALE;
	inputFramebuffer = (uint8*) malloc(baseSize);
	blitFramebuffer = (uint8*) malloc(baseSize);
	outputFramebuffer = (uint8*) malloc(baseSize * 2);
	memset(outputFramebuffer, 0xff, baseSize * 2);

	// Skip 2 rows!?
	GFX.Screen = (uint16*) (inputFramebuffer + (GFX.Pitch * 2 * GFX_SCALE));
	S9xGraphicsInit();

	S9xBlitFilterInit();
	S9xBlit2xSaIFilterInit();
	S9xBlitHQ2xFilterInit();
}

void S9xDeinitDisplay(void) {
	free(inputFramebuffer);
	inputFramebuffer = NULL;
	free(outputFramebuffer);
	outputFramebuffer = NULL;
	S9xGraphicsDeinit();
}

void blitToOutput(uint32 width, uint32 height, uint32 pitch) {
	uint16* rowStart = (uint16*) blitFramebuffer;
	uint8* out = outputFramebuffer;
	uint16* end = &rowStart[width * height];
	while (rowStart < end) {
		for (int i = 0; i < width; i++) {
			uint16 pixel = rowStart[i];
			out[0] = ( ( pixel >> 11) & 0x1f ) << 3;
			out[1] = ( ( pixel >> 5 ) & 0x3f ) << 2;
			out[2] = ( ( pixel      ) & 0x1f ) << 3;
			out += 4;
		}
		rowStart += pitch >> 1;
	}
}

static uint32 prevWidth = 0;
static uint32 prevHeight = 0;

void S9xPutImage(int width, int height) {
	int copyWidth, copyHeight;
	Blitter blitFn = NULL;

	if (videoMode == VIDEOMODE_BLOCKY || videoMode == VIDEOMODE_TV || videoMode == VIDEOMODE_SMOOTH) {
		if ((width <= SNES_WIDTH) && ((prevWidth != width) || (prevHeight != height))) {
			S9xBlitClearDelta();
		}
	}

	if (width <= SNES_WIDTH) {
		if (height > SNES_HEIGHT_EXTENDED) {
			copyWidth  = width * 2;
			copyHeight = height;
			blitFn = S9xBlitPixSimple2x1;
		} else {
			copyWidth  = width  * 2;
			copyHeight = height * 2;

			switch (videoMode) {
				case VIDEOMODE_BLOCKY:		blitFn = S9xBlitPixSimple2x2;		break;
				case VIDEOMODE_TV:			blitFn = S9xBlitPixTV2x2;			break;
				case VIDEOMODE_SMOOTH:		blitFn = S9xBlitPixSmooth2x2;		break;
				case VIDEOMODE_SUPEREAGLE:	blitFn = S9xBlitPixSuperEagle16;	break;
				case VIDEOMODE_2XSAI:		blitFn = S9xBlitPix2xSaI16;			break;
				case VIDEOMODE_SUPER2XSAI:	blitFn = S9xBlitPixSuper2xSaI16;	break;
				case VIDEOMODE_EPX:			blitFn = S9xBlitPixEPX16;			break;
				case VIDEOMODE_HQ2X:		blitFn = S9xBlitPixHQ2x16;			break;
			}
		}
	} else if (height <= SNES_HEIGHT_EXTENDED) {
		copyWidth  = width;
		copyHeight = height * 2;

		switch (videoMode) {
			default:	blitFn = S9xBlitPixSimple1x2;	break;
			case VIDEOMODE_TV:	blitFn = S9xBlitPixTV1x2;		break;
		}
	} else {
		copyWidth  = width;
		copyHeight = height;
		blitFn = S9xBlitPixSimple1x1;
	}
	blitFn((uint8 *) GFX.Screen, GFX.Pitch, blitFramebuffer, GFX.Pitch, width, height);

	if (height < prevHeight) {
		int	p = GFX.Pitch >> 2;
		for (int y = SNES_HEIGHT * GFX_SCALE; y < SNES_HEIGHT_EXTENDED * GFX_SCALE; y++) {
			uint32	*d = (uint32 *) (outputFramebuffer + y * GFX.Pitch);
			for (int x = 0; x < p; x++) {
				*d++ = 0;
			}
		}
	}

	blitToOutput(copyWidth, copyHeight, GFX.Pitch);

	EM_ASM_({
		SNES9X.display.draw($0, $1, $2);
	}, outputFramebuffer, copyWidth, copyHeight);

	prevHeight = height;
	prevWidth = width;
}

void S9xMessage(int type, int number, const char *message) {
	const int	max = 36 * 3;
	static char	buffer[max + 1];

	fprintf(stdout, "%s\n", message);
	strncpy(buffer, message, max + 1);
	buffer[max] = 0;
	S9xSetInfoString(buffer);
}

const char* S9xStringInput (const char *message) {
	static char	buffer[256];

	printf("%s: ", message);
	fflush(stdout);

	if (fgets(buffer, sizeof(buffer) - 2, stdin))
		return (buffer);

	return (NULL);
}

void S9xSetTitle(const char *string) {
}

void S9xSetPalette(void) {
	return;
}
