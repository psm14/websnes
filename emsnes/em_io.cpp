#include <ctype.h>
#include <sys/types.h>
#include "conffile.h"
#include <emscripten.h>

#include "em_io.h"

bool8 LoadSRAMMem(uint8* data, uint32 len) {
	Memory.ClearSRAM();

	if (Multi.cartType && Multi.sramSizeB) {
		return FALSE;
	}

	int size = Memory.SRAMSize ? (1 << (Memory.SRAMSize + 3)) * 128 : 0;
	if (size > 0x20000)
		size = 0x20000;

	if (size)
	{
		memcpy(Memory.SRAM, data, len);
		if (len - size == 512)
			memmove(Memory.SRAM, Memory.SRAM + 512, size);

		// TODO: Support
		// if (Settings.SRTC || Settings.SPC7110RTC)
		//   LoadSRTC();

		return TRUE;
	}
	return FALSE;
}

bool8 SaveSRAMMem() {
	if (Settings.SuperFX && Memory.ROMType < 0x15) // doesn't have SRAM
		return TRUE;

	if (Settings.SA1 && Memory.ROMType == 0x34)    // doesn't have SRAM
		return TRUE;

	if (Multi.cartType && Multi.sramSizeB) {
		return FALSE;
  }

  int size = Memory.SRAMSize ? (1 << (Memory.SRAMSize + 3)) * 128 : 0;
	if (size > 0x20000)
		size = 0x20000;

	if (size) {
    EM_ASM_({
		  SNES9X.fileio.onSRAMSaved($0, $1);
	  }, Memory.SRAM, size);

		// TODO: SRTC support
		//	if (Settings.SRTC || Settings.SPC7110RTC)
				// SaveSRTC();

		return TRUE;
	}
	return FALSE;
}
