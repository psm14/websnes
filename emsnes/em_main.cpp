#include <ctype.h>
#include <sys/types.h>
#include <emscripten.h>
#include "port.h"
#include "conffile.h"
#include "snes9x.h"
#include "memmap.h"
#include "apu/apu.h"
#include "gfx.h"
#include "controls.h"
#include "snapshot.h"
#include "display.h"
#include "em_io.h"

static void NSRTControllerSetup(void);

void _makepath(char *path, const char *, const char *dir, const char *fname, const char *ext) {
}

void _splitpath(const char *path, char *drive, char *dir, char *fname, char *ext) {
}

void S9xExtraUsage(void) {
}

const char *S9xChooseMovieFilename (bool8 read_only){
    return NULL;
}

static void NSRTControllerSetup (void)
{
	if (!strncmp((const char *) Memory.NSRTHeader + 24, "NSRT", 4))
	{
		// First plug in both, they'll change later as needed
		S9xSetController(0, CTL_JOYPAD, 0, 0, 0, 0);
		S9xSetController(1, CTL_JOYPAD, 1, 0, 0, 0);

		switch (Memory.NSRTHeader[29])
		{
			case 0x00:	// Everything goes
				break;

			case 0x10:	// Mouse in Port 0
				S9xSetController(0, CTL_MOUSE,      0, 0, 0, 0);
				break;

			case 0x01:	// Mouse in Port 1
				S9xSetController(1, CTL_MOUSE,      1, 0, 0, 0);
				break;

			case 0x03:	// Super Scope in Port 1
				S9xSetController(1, CTL_SUPERSCOPE, 0, 0, 0, 0);
				break;

			case 0x06:	// Multitap in Port 1
				S9xSetController(1, CTL_MP5,        1, 2, 3, 4);
				break;

			case 0x66:	// Multitap in Ports 0 and 1
				S9xSetController(0, CTL_MP5,        0, 1, 2, 3);
				S9xSetController(1, CTL_MP5,        4, 5, 6, 7);
				break;

			case 0x08:	// Multitap in Port 1, Mouse in new Port 1
				S9xSetController(1, CTL_MOUSE,      1, 0, 0, 0);
				// There should be a toggle here for putting in Multitap instead
				break;

			case 0x04:	// Pad or Super Scope in Port 1
				S9xSetController(1, CTL_SUPERSCOPE, 0, 0, 0, 0);
				// There should be a toggle here for putting in a pad instead
				break;

			case 0x05:	// Justifier - Must ask user...
				S9xSetController(1, CTL_JUSTIFIER,  1, 0, 0, 0);
				// There should be a toggle here for how many justifiers
				break;

			case 0x20:	// Pad or Mouse in Port 0
				S9xSetController(0, CTL_MOUSE,      0, 0, 0, 0);
				// There should be a toggle here for putting in a pad instead
				break;

			case 0x22:	// Pad or Mouse in Port 0 & 1
				S9xSetController(0, CTL_MOUSE,      0, 0, 0, 0);
				S9xSetController(1, CTL_MOUSE,      1, 0, 0, 0);
				// There should be a toggles here for putting in pads instead
				break;

			case 0x24:	// Pad or Mouse in Port 0, Pad or Super Scope in Port 1
				// There should be a toggles here for what to put in, I'm leaving it at gamepad for now
				break;

			case 0x27:	// Pad or Mouse in Port 0, Pad or Mouse or Super Scope in Port 1
				// There should be a toggles here for what to put in, I'm leaving it at gamepad for now
				break;

			// Not Supported yet
			case 0x99:	// Lasabirdie
				break;

			case 0x0A:	// Barcode Battler
				break;
		}
	}
}

void S9xParsePortConfig(ConfigFile &conf, int pass) {
}

const char* S9xGetDirectory(enum s9x_getdirtype dirtype)
{
	return "/";
}

const char * S9xGetFilename(const char *ex, enum s9x_getdirtype dirtype)
{
	return "/file";
}

const char * S9xGetFilenameInc(const char *ex, enum s9x_getdirtype dirtype)
{
	return "/file";
}

const char * S9xBasename(const char *f)
{
	const char	*p;

	if ((p = strrchr(f, '/')) != NULL || (p = strrchr(f, '\\')) != NULL)
		return (p + 1);

	return (f);
}

const char * S9xSelectFilename(const char *def, const char *dir1, const char *ext1, const char *title) {
	return NULL;
}

const char * S9xChooseFilename(bool8 read_only) {
	return NULL;
}


bool8 S9xOpenSnapshotFile(const char *filename, bool8 read_only, STREAM *file) {
	return FALSE;
}

void S9xCloseSnapshotFile(STREAM file) {
}

bool8 S9xInitUpdate(void) {
	return TRUE;
}

bool8 S9xDeinitUpdate(int width, int height) {
	S9xPutImage(width, height);
	return TRUE;
}

bool8 S9xContinueUpdate(int width, int height) {
	return TRUE;
}

void S9xAutoSaveSRAM(void) {
		SaveSRAMMem();
    printf("auto save sram\n");
}

EMSCRIPTEN_KEEPALIVE
extern "C" void auto_save_sram() {
	S9xAutoSaveSRAM();
}

void S9xSyncSpeed(void) {
	IPPU.RenderThisFrame = (++IPPU.SkippedFrames >= Settings.SkipFrames) ? TRUE : FALSE;
  if (IPPU.RenderThisFrame)
		IPPU.SkippedFrames = 0;

	// TODO: Auto frameskip and requestAnumationFrame limiting.
}

void S9xExit(void) {
	S9xSetSoundMute(TRUE);
	Settings.StopEmulation = TRUE;

#ifdef NETPLAY_SUPPORT
	if (Settings.NetPlay)
		S9xNPDisconnect();
#endif

  Memory.SaveSRAM(S9xGetFilename(".srm", SRAM_DIR));

	S9xUnmapAllControls();
	S9xDeinitDisplay();
	Memory.Deinit();
	S9xDeinitAPU();

	exit(0);
}

#ifdef DEBUGGER
static void sigbrkhandler(int) {
	CPU.Flags |= DEBUG_MODE_FLAG;
	signal(SIGINT, (SIG_PF) sigbrkhandler);
}
#endif

void S9xParseArg(char **argv, int &i, int argc) {
}

EMSCRIPTEN_KEEPALIVE
extern "C" void toggle_display_framerate() {
    Settings.DisplayFrameRate = !Settings.DisplayFrameRate;
}

static bool soundSync = false;

EMSCRIPTEN_KEEPALIVE
extern "C" void mainloop() {
		soundSync = S9xSyncSound();
		if (soundSync) {
  	  S9xProcessEvents(FALSE);
  	  S9xMainLoop();
		} else {
			printf("Sound out of sync!\n");
		}
}

void reboot_emulator(uint8* rom, uint32 length) {
  uint32 saved_flags = CPU.Flags;
	bool8	loaded = FALSE;
  loaded = Memory.LoadROMMem(rom, length);

	if (!loaded) {
		fprintf(stderr, "Error opening the ROM file.\n");
		exit(1);
	}

	NSRTControllerSetup();

	bool8 sramloaded = false;
	uint8* sram = (uint8*) EM_ASM_INT({
		return SNES9X.fileio.loadSRAM();
	}, 0);
	if (sram) {
		uint32 length = (uint32) EM_ASM_INT({
			return SNES9X.fileio.getLength($0);
		}, (void*) sram);
		sramloaded = LoadSRAMMem(sram, length);
		EM_ASM_({
			SNES9X.fileio.freeBuffer($0);
		}, sram);
	}
	if (!sramloaded) {
		printf("SRAM load failed.\n");
	}

	CPU.Flags = saved_flags;
	Settings.StopEmulation = FALSE;

	S9xInitInputDevices();
	S9xInitDisplay(NULL, NULL);
	S9xSetSoundMute(FALSE);
}

EMSCRIPTEN_KEEPALIVE
extern "C" void run(uint8* rom){
	uint32 length = (uint32) EM_ASM_INT({
		return SNES9X.fileio.getLength($0);
	}, (void*) rom);
  reboot_emulator(rom, length);
  printf("S9xSetSoundMute(FALSE)\n");
  S9xSetSoundMute(FALSE);
}

EMSCRIPTEN_KEEPALIVE
extern "C" int main (int argc, char **argv)
{
	printf("\n\nSnes9x " VERSION " for emscripten\n");

	memset(&Settings, 0, sizeof(Settings));
	Settings.MouseMaster = TRUE;
	Settings.SuperScopeMaster = TRUE;
	Settings.JustifierMaster = TRUE;
	Settings.MultiPlayer5Master = FALSE;
	Settings.FrameTimePAL = 20000;
	Settings.FrameTimeNTSC = 16667;
	Settings.SixteenBitSound = TRUE;
	Settings.Stereo = TRUE;
	Settings.SupportHiRes = TRUE;
	Settings.Transparency = TRUE;
	Settings.AutoDisplayMessages = FALSE;
	Settings.InitialInfoStringTimeout = 120;
	Settings.HDMATimingHack = 100;
	Settings.BlockInvalidVRAMAccessMaster = TRUE;
	Settings.StopEmulation = TRUE;
	Settings.WrongMovieStateProtection = TRUE;
	Settings.DumpStreamsMaxFrames = -1;
  Settings.DisplayFrameRate = FALSE;
	Settings.StretchScreenshots = 1;
	Settings.SnapshotScreenshots = TRUE;
	Settings.SkipFrames = 0;
	Settings.TurboSkipFrames = 15;
	Settings.CartAName[0] = 0;
	Settings.CartBName[0] = 0;
	Settings.NoPatch = TRUE;
	Settings.SoundSync = FALSE;
	Settings.AutoSaveDelay = 1;
	Settings.MaxSpriteTilesPerLine = 34;
	Settings.OneClockCycle = 6;
	Settings.OneSlowClockCycle = 8;
	Settings.TwoClockCycles = 12;
	Settings.SuperFXClockMultiplier = 100;

	// TODO: Untangle this mess
	uint32 sampleRate = (uint32) EM_ASM_INT({
		return SNES9X.audio.sampleRate();
	}, 0);

	Settings.Mute = FALSE;
  Settings.SoundPlaybackRate = sampleRate;
	Settings.SoundInputRate = 31950;
	Settings.DynamicRateControl = TRUE;
	Settings.DynamicRateLimit = 1000;
	Settings.InterpolationMethod = DSP_INTERPOLATION_GAUSSIAN;

	CPU.Flags = 0;    ;
	if (!Memory.Init() || !S9xInitAPU())
	{
		fprintf(stderr, "Snes9x: Memory allocation failure - not enough RAM/virtual memory available.\nExiting...\n");
		Memory.Deinit();
		S9xDeinitAPU();
		exit(1);
	}
	S9xInitSound(200 /*ms*/, 0 /* lag ms */);
	S9xSetSoundMute(TRUE);

	S9xReportControllers();

#ifdef GFX_MULTI_FORMAT
	S9xSetRenderPixelFormat(RGB565);
#endif

	EM_ASM({
		if (SNES9X.onSnesLoad) {
			SNES9X.onSnesLoad();
		}
	});

	emscripten_exit_with_live_runtime();
	return (0);
}
