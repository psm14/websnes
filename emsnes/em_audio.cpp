#include <ctype.h>
#include <sys/types.h>
#include <emscripten.h>
#include "apu/apu.h"

// Bigger than we'll need
static uint8 soundBuffer[32768];
static bool muted = TRUE;

static void samples_available(void* data) {
  S9xFinalizeSamples();
  if (muted) {
    S9xClearSamples();
  }
}

EMSCRIPTEN_KEEPALIVE
extern "C" bool is_muted() {
  return muted;
}

EMSCRIPTEN_KEEPALIVE
extern "C" void set_muted(bool isMuted) {
  muted = isMuted;
}

EMSCRIPTEN_KEEPALIVE
extern "C" uint32 num_samples_available() {
  // Divide by 4 because stereo and 8/16 bit
  return S9xGetSampleCount() >> 2;
}

EMSCRIPTEN_KEEPALIVE
extern "C" void* fill_sample_buffer(uint32 samples) {
  // Double because stereo
  S9xMixSamples(soundBuffer, samples << 1);
  return soundBuffer;
}

bool8 S9xOpenSoundDevice(void) {
  S9xSetSamplesAvailableCallback(samples_available, NULL);
  return TRUE;
}

void S9xToggleSoundChannel(int c) {
	static uint8	sound_switch = 255;

	if (c == 8)
		sound_switch = 255;
	else
		sound_switch ^= 1 << c;

	S9xSetSoundControl(sound_switch);
}
