#ifndef _EM_IO_H_
#define _EM_IO_H_

#include "memmap.h"

bool8 LoadSRAMMem(uint8*, uint32);
bool8 SaveSRAMMem();

#endif
