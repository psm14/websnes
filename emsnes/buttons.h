#ifndef BUTTONS_H
#define BUTTONS_H

#define NUM_BUTTONS 12

#define BTN_RIGHT  0
#define BTN_LEFT   1
#define BTN_DOWN   2
#define BTN_UP     3
#define BTN_START  4
#define BTN_SELECT 5
#define BTN_A      6
#define BTN_B      7
#define BTN_X      8
#define BTN_Y      9
#define BTN_L      10
#define BTN_R      11

#endif
