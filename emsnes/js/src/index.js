import Audio from './Audio';
import FileIO from './FileIO';
import Display from './Display';

SNES9X.audio = new Audio(SNES9X);
SNES9X.fileio = new FileIO(SNES9X);
SNES9X.display = new Display(SNES9X);
