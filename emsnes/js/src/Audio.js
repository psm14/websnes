const scaleSample = (sample) => sample / 32768

class Audio {
  constructor(SNES9X, bufferSize) {
    this.SNES9X = SNES9X;

    let CtxClazz = window.AudioContext || window.webkitAudioContext;
    this.context = new CtxClazz({ sampleRate: 44100 });

    // 16-bit samples per channel
    this.bufferSize = bufferSize || 2048;
    this.audioListener = this.fillBuffer.bind(this);
  }
  
  isMuted() {
    // Coerce to boolean
    return this.SNES9X._is_muted() ? true : false;
  }

  setMute(mute) {
    this.setMuteInternal(mute);
    this.SNES9X._set_muted(mute ? 1 : 0);
  }

  setMuteInternal(mute) {
    if (this.processingNode) {
      this.processingNode.removeEventListener('audioprocess', this.audioListener);
      this.processingNode.disconnect();
      this.processingNode = null;
    }
    if (!mute) {
      this.context.resume();
      this.processingNode = this.context.createScriptProcessor(this.bufferSize, 0, 2);
      this.processingNode.addEventListener('audioprocess', this.audioListener);
      this.processingNode.connect(this.context.destination);
    } else {
      this.context.suspend();
    }
  }

  sampleRate() {
    return this.context.sampleRate;
  }

  syncAudio() {
    this.setMuteInternal(this.isMuted());
  }

  fillBuffer(e) {
    let numSamples = this.SNES9X._num_samples_available();

    if (this.mute && numSamples > 0) {
      // Just fill the buffer
      this.SNES9X._fill_sample_buffer(this.bufferSize);
      return;
    }

    if (numSamples < this.bufferSize) { return; }

    let left = e.outputBuffer.getChannelData(0);
    let right = e.outputBuffer.getChannelData(1);

    let pointer = this.SNES9X._fill_sample_buffer(this.bufferSize);
    let buffer = this.SNES9X.HEAP16;

    let offset = pointer >> 1;
    for (let i = 0; i < this.bufferSize; i++) {
      left[i] = scaleSample(buffer[offset++]);
      right[i] = scaleSample(buffer[offset++]);
    }
  }
}

export default Audio;
