const parseAttr = (element, attr) => {
  let value = element.getAttribute(attr);
  return value ? parseInt(value) : 0;
}

class Display {
  constructor(SNES9X) {
    this.SNES9X = SNES9X;
  }

  videoMode() {
    return this.SNES9X._get_video_mode();
  }

  setVideoMode(mode) {
    if (mode > 0 && mode <= 8) {
      this.SNES9X._set_video_mode(mode);
    }
  }

  initContext() {
    this.context = this.canvas.getContext('2d');
    this.lastWidth = parseAttr(this.canvas, 'width');
    this.lastHeight = parseAttr(this.canvas, 'height');
    this.lastPointer = 0;
    this.imageData = null;
  }

  clearContext() {
    this.context = null;
    this.lastWidth = 0;
    this.lastHeight = 0;
    this.lastPointer = 0;
    this.imageData = null;
  }

  clearListeners() {
    if (this.canvas && this.lossListener) {
      this.canvas.removeEventListener('webglcontextlost', this.lossListener);
    }
    if (this.canvas && this.restoreListener) {
      this.canvas.removeEventListener('webglcontextrestored', this.restoreListener);
    }
  }

  updateCanvas(canvas) {
    this.canvas = canvas;
    if (canvas) {
      // TODO: Does this even happen with a 2D drawing context?
      this.lossListener = (e) => {
        console.log("WebGL context lost.");
        this.clearContext();
        e.preventDefault();
      }
      this.restoreListener = (e) => {
        console.log("WebGL context restored.");
        this.initContext();
        e.preventDefault();
      }
      this.canvas.addEventListener('webglcontextlost', this.lossListener);
      this.canvas.addEventListener('webglcontextrestored', this.restoreListener);
      this.initContext();
    } else {
      this.clearContext();
    }
  }

  getImageData(pointer, width, height) {
    if (this.lastImageData && this.lastWidth === width && this.lastHeight === height && this.lastPointer === pointer) {
      return this.lastImageData;
    } else {
      let length = width * height * 4;
      let buffer = new Uint8ClampedArray(this.SNES9X.HEAPU8.buffer, pointer, length);
      let imageData = new ImageData(buffer, width, height);
      this.lastWidth = width;
      this.lastHeight = height;
      this.lastPointer = pointer;
      this.lastImageData = imageData;
      this.canvas.setAttribute('width', width);
      this.canvas.setAttribute('height', height);
      return imageData;
    }
  }

  draw(pointer, width, height) {
    if (!this.context) {
      // No active canvas
      return;
    }
    let imageData = this.getImageData(pointer, width, height);
    this.context.putImageData(imageData, 0, 0);
  }
}

export default Display;
