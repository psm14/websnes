class FileIO {
  constructor(SNES9X) {
    this.SNES9X = SNES9X;
    this.injects = {};
    this._saveData = null;
    this._loadData = null;
  }

  injectBuffer(buffer) {
    let pointer = this.SNES9X._malloc(buffer.length);
    let dest = new Uint8Array(this.SNES9X.HEAPU8.buffer, pointer, buffer.length);
    dest.set(buffer);
    this.injects[pointer] = buffer.length;
    return pointer;
  }

  freeBuffer(pointer) {
    if (this.injects[pointer]) {
      this.SNES9X._free(pointer);
      delete this.injects[pointer];
    }
  }

  getLength(pointer) {
    return this.injects[pointer] || 0;
  }

  onSRAMSaved(pointer, len) {
    if (this.saveData) {
      let buffer = new Uint8Array(this.SNES9X.HEAPU8.buffer, pointer, len);
      let copy = new Uint8Array(len);
      copy.set(buffer);
      this.saveData(copy);
    }
  }

  loadSRAM() {
    if (!this.loadData) {
      return 0;
    }

    let buffer = this.loadData();
    if (!buffer) {
      return 0;
    } else {
      return this.injectBuffer(buffer);
    }
  }
}

export default FileIO;
